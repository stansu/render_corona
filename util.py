import bpy
import random
import multiprocessing
import os, datetime
import math, mathutils
from bpy_extras.io_utils    import axis_conversion
from extensions_framework   import util as efutil
from shutil                 import copyfile
from math                   import tan, atan, degrees
from .                      import bl_info
       
#------------------------------------
# Module settings.
#------------------------------------
Verbose = True
EnableDebug = False

sep = os.path.sep

def debug( *args):
    global EnableDebug
    if EnableDebug:
        msg = ' '.join(['%s'%a for a in args])
        print( "debug:  ", msg)
    else:
        pass

def CrnUpdate( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Corona:", msg)

def CrnProgress( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Progress:", msg)    

def CrnInfo( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "Info:", msg)

def CrnError( *args):
    msg = ' '.join(['%s'%a for a in args])
    print( "------")
    print( "Error:", msg)
    print( "------")

    
#------------------------------------
# Generic utilities and settings.
#------------------------------------
script_name = "render_corona"

# Addon directory.
addon_paths = bpy.utils.script_paths("addons")
if 'render_corona' in os.listdir(addon_paths[0]):
    addon_dir = os.path.join( addon_paths[0], 'render_corona')
else:
    addon_dir = os.path.join( addon_paths[1], 'render_corona')

# Total system threads.
thread_count = multiprocessing.cpu_count() 

sep = os.path.sep

def random_seed():
    return random.randrange(1, 2000)

def get_version_string():
    return "version " + ".".join(map(str, bl_info["version"]))

def strip_spaces( name):
    return ('_').join( name.split(' '))

def join_names_underscore( name1, name2):
    return ('_').join( (strip_spaces( name1), strip_spaces( name2)))

def join_params( params, directive):
    return ('').join( (('').join( params), directive))

def filter_flatten_params( params):
    filter_list = []
    for p in params:
        if p not in filter_list:
            filter_list.append( p)
    return ('').join( filter_list)
        
def get_timestamp():
    now = datetime.datetime.now()
    return "%d-%d-%d %d:%d:%d\n" % (now.month, now.day, now.year, now.hour, now.minute, now.second)

    
def findVertexGroupName(face, vWeightMap):
    """
    Searches the vertexDict to see what groups is assigned to a given face.
    We use a frequency system in order to sort out the name because a given vetex can

    belong to two or more groups at the same time. To find the right name for the face
    we list all the possible vertex group names with their frequency and then sort by
    frequency in descend order. The top element is the one shared by the highest number
    of vertices is the face's group

    """
    weightDict = {}
    for vert_index in face.vertices:
        vWeights = vWeightMap[vert_index]
        for vGroupName, weight in vWeights:
            weightDict[vGroupName] = weightDict.get(vGroupName, 0.0) + weight

    if weightDict:
        return max((weight, vGroupName) for vGroupName, weight in weightDict.items())[1]
    else:
        return '(null)'


def in_scene_layer(ob, scene):
    for i in range(len(ob.layers)):
        if ob.layers[i] and scene.layers[i]:
            return True
        else:
            continue
        
def do_export(ob, scene):
    return not ob.hide_render and ob.type in ('MESH', 'SURFACE', 'META', 'TEXT', 'CURVE') and in_scene_layer(ob, scene)

def in_local_layer(ob, layers):
    for i in range( len(ob.layers_local_view)):
        if ob.layers_local_view[i] and layers[i]:
            return True
        else:
            continue
            
def crn_nodes(material):    
            if material.use_nodes:
                for node in material.node_tree.nodes:
                    if node:
                        if hasattr(node, 'node_tree'):
                            if 'Corona Nodetree' in node.node_tree.name:
                                return node
                    
def realpath(path):
    return os.path.realpath(efutil.filesystem_path(path))

def view3D_mat_update(self, context):
    context.object.active_material.diffuse_color = context.object.active_material.corona.kd
    
def scene_enumerator(self, context):
    matches = []
    for scene in bpy.data.scenes:
        matches.append((scene.name, scene.name, ""))
    return matches

def camera_enumerator(self, context):
    return object_enumerator('CAMERA')

def object_enumerator(self, context):
    matches = []
    for object in context.scene.objects:
        if object.type in {'MESH', 'SURFACE'}:
            matches.append((object.name, object.name, ""))
    return matches

def sun_enumerator(self, context):
    sun = []
    for object in context.scene.objects:
        if object.type == 'LAMP':
            if object.data.type == 'SUN':
                sun.append((object.name, object.name, ""))
    return sun


def node_enumerator( self, context):
    nodes = []
    tree = bpy.data.node_groups[ context.object.active_material.corona.node_tree]
    for node in tree.nodes:
        if node.bl_idname in { 'CoronaMtlNode', 'CoronaLightMtlNode'}:
            nodes.append((node.name, node.name, ""))
    return nodes


def mat_enumerator(self, context):
    materials = []
    for material in bpy.data.materials:
        materials.append((material.name, material.name, ""))
    return materials

def is_uv_img( texture_name):
    texture  = bpy.data.textures[texture_name]
    if bool(texture) and hasattr(texture, "image"):
        if texture.image.file_format in {'OPEN_EXR', 'PNG', 'BMP', 'JPEG', 'TIFF', 'TARGA'}:
            return True
        else:
            CrnInfo("%s format not supported, skipping image texture %s" % ( texture.image.file_format, texture.image.name))
    else:
        return False

def get_tex_img(texture_name):
    return bpy.data.textures[texture_name].image.filepath.split(sep)[-1]

def get_tex_path(texture_name):
    return realpath(bpy.data.textures[texture_name].image.filepath)

def is_proxy(ob, scene):
    proxy = False
    if ob.type == 'MESH' and ob.corona.is_proxy:
        if ob.corona.use_external:
            proxy = ob.corona.external_instance_mesh != ''
        elif ob.corona.instance_mesh != '':
            proxy = scene.objects[ob.corona.instance_mesh].type == 'MESH'
    return proxy
            
def get_all_psysobs():
    obs = set()
    for settings in bpy.data.particles:
        if settings.render_type == 'OBJECT' and settings.dupli_object is not None:
            obs.add( settings.dupli_object)
        elif settings.render_type == 'GROUP' and settings.dupli_group is not None:
            obs.update( {ob for ob in settings.dupli_group.objects})
    return obs

def get_all_duplis( scene ):
    obs = set()
    for ob in scene.objects:
        if ob.parent and ob.parent.dupli_type in {'FACES', 'VERTS', 'GROUP'}:
            obs.add( ob)
    return obs
            
def get_matrix(obj , grp=False):
    obj_mat = obj.matrix.copy()
    return obj_mat

def get_instances(obj_parent, scene):
    crn_scn = scene.corona
    dupli_list = []
    if use_ob_mb( obj_parent, scene):
        dupli_list = sample_mblur( obj_parent, scene, dupli = True)
    else:
        obj_parent.dupli_list_create( scene)
        for dupli in obj_parent.dupli_list :
            dupli_matrix = get_matrix( dupli, grp=(obj_parent.dupli_type == 'GROUP')) 
            dupli_list.append( [ dupli.object, [dupli_matrix]])
        obj_parent.dupli_list_clear()
    return dupli_list

def is_psys_emitter( ob):
    emitter = False
    if hasattr( ob, "modifiers"):
        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.render_type == 'OBJECT' and psys.settings.dupli_object is not None:
                    emitter = True
                    break
                if psys.settings.render_type == 'GROUP' and psys.settings.dupli_group is not None:
                    emitter = True
                    break
    return emitter
                
def render_emitter(ob):
    render = False
    for psys in ob.particle_systems:
        if psys.settings.use_render_emitter:
            render = True
            break
    return render

def is_obj_visible(scene, obj, is_dupli=False):
	ov = False
	for lv in [ol and sl and rl for ol,sl,rl in zip(obj.layers, scene.layers, scene.render.layers.active.layers)]:
		ov |= lv
	return (ov or is_dupli) and not obj.hide_render

def get_instance_materials(ob):
	obmats = []
	# Grab materials attached to object instances ...
	if hasattr(ob, 'material_slots'):
		for ms in ob.material_slots:
			obmats.append(ms.material)
	# ... and to the object's mesh data
	if hasattr(ob.data, 'materials'):
		for m in ob.data.materials:
			obmats.append(m)
	return obmats

def has_hairsys( ob):
    has_hair = False
    for mod in ob.modifiers:
        if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
            psys = mod.particle_system
            if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                has_hair = True
                break
    return has_hair

def use_def_mb( ob, scene):
    return scene.corona.use_def_mblur and ob.corona.use_mblur and ob.corona.mblur_type == 'deform'

def use_ob_mb( ob, scene):
    return scene.corona.use_ob_mblur and ob.corona.use_mblur and ob.corona.mblur_type == 'object'

    
def sample_mblur( ob, scene, dupli = False):
    matrices = []
    frame_orig = scene.frame_current
    frame_set = scene.frame_set

    if ob.type == 'CAMERA':
        num_segs = scene.corona.cam_mblur_segments
    else:
        num_segs = scene.corona.ob_mblur_segments
    offset = 0.5 + ( scene.corona.frame_offset / 2)
    interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
    start = frame_orig - ( interval * ( 1 - offset))
    end = frame_orig + ( interval * offset)
    frac = interval / (num_segs + 1)
    if not dupli:
        # Set frames and collect matrices.
        # Have to set subframe separately.
        frame_set( int(start), subframe = ( start % 1))
        if ob.type == 'CAMERA':
            matrices.append( calc_cam_shift( ob, scene))
        else:
            matrices.append( ob.matrix_world.copy())
        frame = start
        for i in range( 0, (num_segs + 1)):
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            if ob.type == 'CAMERA':
                matrices.append( calc_cam_shift( ob, scene))
            else:
                matrices.append( ob.matrix_world.copy())
    elif dupli:
        frame_set( int(start), subframe = ( start % 1))
        ob.dupli_list_create( scene)
        for dupli in ob.dupli_list:
            matrices.append( [dupli.object, [dupli.matrix.copy(),]])
        ob.dupli_list_clear()
        frame = start
        for i in range( 0, (num_segs + 1)):
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            ob.dupli_list_create( scene)
            for m in range( 0, len(ob.dupli_list)):
                matrices[m][1].append( ob.dupli_list[m].matrix.copy())
            ob.dupli_list_clear()
    # Reset timeline and return.
    frame_set( frame_orig)
    return matrices
    
def get_psys_instances(ob, scene):
    ''' 
    Return a dictionary of 
    particle: [dupli.object, [matrices]] 
    pairs. This function assumes particle systems and 
    face / verts duplication aren't being used on the same object.
    '''
    all_dupli_obs = {}
    current_total = 0
    index = 0
    use_mblur = use_ob_mb( ob, scene)
    if not hasattr(ob, 'modifiers'):
        return {}
    if not use_mblur:   
        # If no motion blur
        ob.dupli_list_create(scene, settings = 'RENDER')
    for modifier in ob.modifiers:
        if modifier.type == 'PARTICLE_SYSTEM' and modifier.show_render:
            psys = modifier.particle_system
            if not psys.settings.render_type in {'OBJECT', 'GROUP'}:
                continue
            if psys.settings.type == 'EMITTER':
                particles = [p for p in psys.particles if p.alive_state == 'ALIVE']
            else:
                particles = [p for p in psys.particles]
            start = current_total
            current_total += len( particles)
            
            if not use_mblur:   
                # No motion blur.
                dupli_obs = []
                for dupli_index in range( start, current_total):
                    # Store the dupli.objects for the current particle system only
                    # ob.dupli_list is created in descending order of particle systems
                    # So it should be reliable to match them this way.
                    dupli_obs.append( ob.dupli_list[dupli_index].object)
                    
                if psys.settings.type == 'EMITTER':
                    p_dupli_obs_pairs = list( zip( particles, dupli_obs))     # Match the particles to the dupli_obs
                    p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_dupli_obs_pairs}  # Create particle:[dupli.object, [matrix]] pairs
                    for particle in p_dupli_obs_dict.keys():
                        size = particle.size
                        loc = particle.location
                        scale = p_dupli_obs_dict[particle][0].scale * size
                        transl = mathutils.Matrix.Translation(( loc))
                        scale = mathutils.Matrix.Scale(scale.x, 4, (1,0,0)) * mathutils.Matrix.Scale(scale.y, 4, (0,1,0)) * mathutils.Matrix.Scale(scale.z, 4, (0,0,1))
                        mat = transl * scale
                        p_dupli_obs_dict[particle][1].append(mat)
                else:
                    dupli_mat_list = [ dupli.matrix.copy() for dupli in ob.dupli_list[ start:current_total]]
                    p_dupli_obs_pairs = list( zip( particles, dupli_obs, dupli_mat_list))
                    p_dupli_obs_dict = { p[0]:[ p[1], [p[2]]] for p in p_dupli_obs_pairs}
            else:
                debug( "Start: %d, Current total: %d" % ( start, current_total))
                # Using motion blur       
                p_dupli_obs_dict, index = sample_psys_mblur( ob, scene, psys, index, start, current_total)
                     
            # Add current particle system to the collection.
            all_dupli_obs.update( p_dupli_obs_dict)  
            
    if not use_mblur:   
        # If no motion blur
        ob.dupli_list_clear()               
    return all_dupli_obs

    
def sample_psys_mblur( ob, scene, psys, index, start, current_total):
    '''
    Return a dictionary of 
    particle: [dupli.object, [matrices]] 
    pairs
    '''
    p_dupli_obs_dict = {}
    frame_orig = scene.frame_current
    frame_set = scene.frame_set
    
    num_segs = scene.corona.ob_mblur_segments
    offset = 0.5 + ( scene.corona.frame_offset / 2)
    interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
    frame_start = frame_orig - ( interval * ( 1 - offset))
#    frame_end = frame_orig + ( interval * offset)
    frac = interval / ( num_segs + 1)

    frame_set( int( frame_start), subframe = ( frame_start % 1))
    if psys.settings.type == 'EMITTER':
        particles = [p for p in psys.particles if p.alive_state == 'ALIVE']
    else: 
        particles = [p for p in psys.particles]
    if len(particles) == 0:
        return False
        
    # Create dupli list.    
    ob.dupli_list_create( scene, settings = 'RENDER')

    frame = frame_start
    
    if psys.settings.type == 'EMITTER':
        # Emitter particle system.
        ob.dupli_list_create( scene, 'RENDER')
        duplis = [dupli.object for dupli in ob.dupli_list]
        p_duplis_pairs = list( zip( particles, duplis))
        p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_duplis_pairs}
        new_index = index
        for f in range( 0, ( num_segs + 1)):
            new_index = index
            frame += frac
            frame_set( int( frame), subframe = ( frame % 1))
            for particle in p_dupli_obs_dict.keys():
                size = particle.size 
                transl = particle.location
                scale = p_dupli_obs_dict[particle][0].scale * size
                transl = mathutils.Matrix.Translation(( transl))
                scale = mathutils.Matrix.Scale( scale.x, 4, (1,0,0)) * mathutils.Matrix.Scale( scale.y, 4, (0,1,0)) * mathutils.Matrix.Scale( scale.z, 4, (0,0,1))
                mat = transl * scale
                p_dupli_obs_dict[particle][1].append( mat)
                new_index += 1
        index += new_index
        del scale, transl, mat
        ob.dupli_list_clear()
        
    else:
        # Hair particle system.
        duplis = []
        ob.dupli_list_create( scene, 'RENDER')
        for dupli_index in range( start, current_total):
            duplis.append( ob.dupli_list[dupli_index].object)
        p_duplis_pairs = list( zip( particles, duplis))
        p_dupli_obs_dict = {p[0]:[ p[1], []] for p in p_duplis_pairs}
        ob.dupli_list_clear()
        new_index = index
        for f in range( 0, (num_segs + 1)):
            new_index = index
            frame += frac 
            # Move to next frame, create dupli_list.
            frame_set( int( frame), subframe = ( frame % 1))
            ob.dupli_list_create( scene, settings = 'RENDER')
            for particle in p_dupli_obs_dict.keys():
                mat = ob.dupli_list[new_index].matrix.copy()
                p_dupli_obs_dict[particle][1].append(mat)
                new_index += 1
            ob.dupli_list_clear()
        index += new_index
        
    frame_set( frame_orig)
    return p_dupli_obs_dict, index

        
def write_transform( file, scene, obj):
    ''' 
    Write transforms for objects to .scn file.
    '''
    scnw = file.write
    crn_scn = scene.corona
    if obj.type == 'CAMERA':
        # Camera transformations.
        if crn_scn.use_cam_mblur:
            # Camera motion blur.
            mat_list = sample_mblur( obj, scene)
            for ob_mat in mat_list:
                for i in range( 0, 3):
                    scnw( '{0} {1} {2} {3} '.format( ob_mat[i][0], 
                                               ob_mat[i][1], 
                                               ob_mat[i][2], 
                                               ob_mat[i][3]))
        else:
            # No camera motion blur.
            ob_mat = calc_cam_shift( obj, scene)
            for i in range( 0, 3):
                scnw( '{0} {1} {2} {3} '.format(ob_mat[i][0], 
                                               ob_mat[i][1], 
                                               ob_mat[i][2], 
                                               ob_mat[i][3]))
    else:
        # Object transformations.
        if use_ob_mb( obj, scene):
            # Transformation motion blur.
            mat_list = sample_mblur( obj, scene)
            scnw( 'transformA %d ' % crn_scn.ob_mblur_segments)
            for ob_mat in mat_list:
                for i in range( 0, 3):
                    scnw( '{0} {1} {2} {3} '.format( ob_mat[i][0], 
                                                   ob_mat[i][1], 
                                                   ob_mat[i][2], 
                                                   ob_mat[i][3]))
        else:
            # No transformation motion blur.
            ob_mat = obj.matrix_world.copy()
            scnw( 'transform ')
            for i in range( 0, 3):
                scnw( '{0} {1} {2} {3} '.format(ob_mat[i][0], 
                                               ob_mat[i][1], 
                                               ob_mat[i][2], 
                                               ob_mat[i][3]))
        scnw( '\n')

def plugin_path():
	return os.path.dirname(realpath(__file__))

def resolution(scene):
	xr = scene.render.resolution_x * scene.render.resolution_percentage / 100.0
	yr = scene.render.resolution_y * scene.render.resolution_percentage / 100.0
	return xr, yr
	
def calc_fov(camera_ob, width, height):
    ''' 
    Calculate horizontal FOV if rendered height is greater than rendered with
    Thanks to NOX exporter developers for this and the next solution 
    '''
    camera_angle = degrees(camera_ob.data.angle)
    if width < height:
        length = 18.0/tan(camera_ob.data.angle/2)
        camera_angle = 2*atan(18.0*width/height/length)
        camera_angle = degrees(camera_angle)
    return camera_angle


def calc_cam_shift(camera_ob, scene):
    width = scene.render.resolution_x #* (scene.render.resolution_percentage / 100)
    height = scene.render.resolution_y #* (scene.render.resolution_percentage / 100)
    matrix_world = camera_ob.matrix_world.copy()
    cam_dir_x = matrix_world[0][2]
    cam_dir_y = matrix_world[2][2]
    cam_dir_z =  matrix_world[1][2]
    cam_dir_up_x = matrix_world[0][1]
    cam_dir_up_y = matrix_world[2][1]
    cam_dir_up_z = matrix_world[1][1]
    cam_dir_right_x = matrix_world[0][0]
    cam_dir_right_y = matrix_world[2][0]
    cam_dir_right_z = matrix_world[1][0]
    camtan = tan(camera_ob.data.angle/2)
    if width < height:
        camtan_x = camtan*width/height
        camtan_y = camtan
    else:
        camtan_x = camtan
        camtan_y = camtan*width/height
    cam_dir_x += cam_dir_up_x * -camera_ob.data.shift_y*camtan*2
    cam_dir_y += cam_dir_up_y * -camera_ob.data.shift_y*camtan*2
    cam_dir_z += cam_dir_up_z * -camera_ob.data.shift_y*camtan*2
    cam_dir_x += cam_dir_right_x * -camera_ob.data.shift_x*camtan*2
    cam_dir_y += cam_dir_right_y * -camera_ob.data.shift_x*camtan*2
    cam_dir_z += cam_dir_right_z * -camera_ob.data.shift_x*camtan*2
    return mathutils.Matrix(((cam_dir_right_x, cam_dir_up_x, cam_dir_x, matrix_world[0][3]),
           (cam_dir_right_z, cam_dir_up_z, cam_dir_z, matrix_world[1][3]),
           (cam_dir_right_y, cam_dir_up_y, cam_dir_y, matrix_world[2][3]),
           (0, 0, 0, 1))).inverted()
    
    

