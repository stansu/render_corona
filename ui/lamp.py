import bpy

#---------------------------------------
# Lamp UI
#---------------------------------------
class CoronaLampPanel( bpy.types.Panel):
    bl_label = "Corona Lamp Info "
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"
    COMPAT_ENGINES = {'CORONA'}

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine
        return renderer == 'CORONA' and context.active_object is not None and context.active_object.type == 'LAMP'

    def draw( self, context):
        layout = self.layout
        ob_data = context.object.data
        if ob_data.type == 'SUN':
            layout.label( "Enable Corona Sun and set sun")
            layout.label( "parameters in the World panel.")
        elif ob_data.type in {'AREA', 'POINT', 'SPOT'}:
            layout.label( "%s lamp type is not supported in Corona." % ("".join([ob_data.type[0], ob_data.type[1:].lower()])))
            layout.label( "Use mesh lights with emissive materials instead.") 
        elif ob_data.type == 'HEMI':
            layout.label( "Hemi lamp type is not supported in Corona.")
            layout.label( "Use environment lighting in the World panel.")

def register():
    bpy.utils.register_class( CoronaLampPanel)
    
def unregister():
    bpy.utils.unregister_class( CoronaLampPanel)
