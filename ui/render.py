import bpy

#---------------------------------------
# Render UI
#---------------------------------------
class CoronaRenderPanelBase( object):
    bl_context = "render"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render
        return renderer.engine == 'CORONA'

#---------------------------------------
# Render buttons UI
#---------------------------------------
class CoronaRenderButtons( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Render"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_scene_props = scene.corona

        row = layout.row( align=True)
        row.operator( "render.render", text = "Render", icon = 'RENDER_STILL')
        if crn_scene_props.vfb_type == '0':
            row.operator( "render.render", text = "Animation", icon = 'RENDER_ANIMATION').animation = True
        row = layout.row( align=True)
        row.operator( "corona.export", text = "Export Objects", icon = 'EXPORT')
        row.operator( "corona.export_mat", text = "Materials", icon = 'MATERIAL')
        row.operator( "corona.export_scene", text = "Configuration", icon = 'FILE')

        row = layout.row()
        row.prop( crn_scene_props, "obj_export_bool")
        if crn_scene_props.obj_export_bool:
            row.prop( crn_scene_props, "obj_export_mode", text = "")

        # Disable .objb export for now, it's buggy
        #layout.prop( crn_scene_props, "binary_obj")
        layout.prop( crn_scene_props, "export_hair")
        layout.prop( crn_scene_props, "local_view_only")
        if not crn_scene_props.local_view_only:
            layout.prop( scene.render, "display_mode")        
        layout.separator()

        layout.prop( crn_scene_props, "export_path", text = "Export Path")

        layout.prop( crn_scene_props, "image_format")
        if crn_scene_props.image_format != '.jpg':
            layout.prop( crn_scene_props, "save_alpha")

#---------------------------------------
# Add dimensions and stamp panels
#---------------------------------------
from bl_ui import properties_render
properties_render.RENDER_PT_dimensions.COMPAT_ENGINES.add( 'CORONA')
properties_render.RENDER_PT_stamp.COMPAT_ENGINES.add( 'CORONA')
del properties_render

#---------------------------------------
# Render settings UI
#---------------------------------------
class CoronaRenderSettingsPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Render Settings"
    COMPAT_ENGINES = {'CORONA'} 

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        # Renderer settings
        # Bucket
        layout.prop( crn_props, "renderer_type")
        layout.separator()
        if crn_props.renderer_type == '0':
            layout.prop( crn_props, "vfb_show_bucket")
            layout.prop( crn_props, "do_aa")
            box_in = layout.box()
            box_in.active = crn_props.do_aa
            
            box_in.label( "Antialiasing:")
            row = box_in.row()
            row.prop( crn_props, "buckets_samples")
            row.prop( crn_props, "buckets_steps")

            row = box_in.row()
            row.prop( crn_props, "buckets_adaptive_threshold")
            row.prop( crn_props, "bucket_size")
                
        # Progressive
        if crn_props.renderer_type == '2':
            layout.prop( crn_props, "limit_prog_time")
            if crn_props.limit_prog_time:
                layout.label( "Time Limit:")
                row = layout.row(align = True)
                row.prop( crn_props, "prog_timelimit_hour", text = "Hours")
                row.prop( crn_props, "prog_timelimit_min", text = "Minutes")
                row.prop( crn_props, "prog_timelimit_sec", text = "Seconds")
            else:
                layout.prop( crn_props, "prog_max_passes")
                

        # VCM/Bidir
        if crn_props.renderer_type == '3':
            row = layout.row()
            row.prop( crn_props, "vcm_mode")
            if crn_props.vcm_mode == '4':
                row.prop( crn_props, "bidir_mis")
        
        # PPM        
        if crn_props.renderer_type == '6':
            row = layout.row()
            row.prop( crn_props, "ppm_samples")
            row.prop( crn_props, "ppm_photons")
            
            row = layout.row()
            row.prop( crn_props, "ppm_initial_rad")
            row.prop( crn_props, "ppm_alpha")  

        layout.separator()
        layout.separator()
        
        layout.label( "Sampling:")
        layout.prop( crn_props, "path_samples")
        layout.prop( crn_props, "max_sample_intensity")
        layout.prop( crn_props, "max_depth")
        layout.prop( crn_props, "random_seed")

        split = layout.split()
        col = split.column()
        col.prop( crn_props, "num_threads")
        col.active = crn_props.auto_threads is False
        
        col = split.column()
        col.prop(crn_props, "auto_threads", text = "Auto Threads")

        row = layout.row()
        row.prop( crn_props, "material_override")
        if crn_props.material_override:
            row.prop( crn_props, "clay_render")
            row = layout.row()
            row.prop( crn_props, "override_material")
            if crn_props.clay_render == True:
                row.active = False

        layout.separator()
        layout.separator()
        
        layout.label( "Light Sampling:")
        layout.prop( crn_props, "arealight_samples")
            
#---------------------------------------
# Render GI UI
#---------------------------------------      
class CoronaGIPanel(bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Secondary Global Illumination"
    COMPAT_ENGINES = {'CORONA'} 
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona
                
        #----------------------------------
        # Global Illumination parameters
        # Secondary solver
        layout.prop( crn_props, "gi_secondarysolver", text = "Solver")
        if crn_props.gi_secondarysolver == '3' and crn_props.gi_primarysolver != '3':
            layout.separator()
            layout.prop( crn_props, "hd_precomp_mult", text = "Precomp Multiplier")
            layout.prop( crn_props, "hd_pt_samples")
            layout.prop( crn_props, "hd_interpolation_count")
            layout.prop( crn_props, "hd_max_records")

            layout.separator()
            
            layout.label("Record Sensitivity:")

            box = layout.box()
            box.prop( crn_props, "hd_sens_direct", text = "Directional")
            box.prop( crn_props, "hd_sens_position", text = "Positional")
            box.prop( crn_props, "hd_sens_normal")

        layout.separator()
        
        row = layout.row()
        row.prop( crn_props, "save_secondary_gi")

        row.prop( crn_props, "load_secondary_gi")  
        layout.prop( crn_props, "gi_secondaryfile")
        
#---------------------------------------
# VFB UI
#---------------------------------------
class CoronaVFBPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Virtual Framebuffer"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona
 
        #------------------------------
        # Virtual framebuffer parameters
        layout.prop( crn_props, "vfb_type", text = "VFB")
        layout.prop( crn_props, "vfb_update", text = "VFB Update Rate [ms]")
        
        split = layout.split()
        col = split.column()
        col.label( "Image Filter:")
        col.prop( crn_props, "filter_width")
        
        col = split.column()
        col.prop( crn_props, "image_filter", text = "")
        col.prop( crn_props, "filter_blur")
        
        layout.prop( crn_props, "renderstamp_use")

#---------------------------------------
# Post-processing UI
#---------------------------------------
class CoronaPostPanel( bpy.types.Panel, CoronaRenderPanelBase):
    bl_label = "Corona Post Processing"
    COMPAT_ENGINES = {'CORONA'}

    def draw( self, context):
        layout = self.layout
        scene = context.scene
        crn_props = scene.corona

        layout.prop( crn_props, "colmap_use_photographic")
        row = layout.row()
        row.prop( crn_props, "colmap_exposure")
        if crn_props.colmap_use_photographic:
            row.active = False
            
        layout.prop( crn_props, "colmap_compression")

        layout.separator()

        layout.prop( crn_props, "colmap_tint")
        layout.prop( crn_props, "colmap_contrast")
        layout.prop( crn_props, "colmap_color_temp")

        
def register():
    bpy.utils.register_class( CoronaPostPanel)
    bpy.utils.register_class( CoronaVFBPanel)
    bpy.utils.register_class( CoronaGIPanel)
    bpy.utils.register_class( CoronaRenderSettingsPanel)
    bpy.utils.register_class( CoronaRenderButtons)
def unregister():
    bpy.utils.unregister_class( CoronaPostPanel)
    bpy.utils.unregister_class( CoronaVFBPanel)
    bpy.utils.unregister_class( CoronaGIPanel)
    bpy.utils.unregister_class( CoronaRenderSettingsPanel)
    bpy.utils.unregister_class( CoronaRenderButtons)
