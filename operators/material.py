import bpy
from mathutils  import Color
from ..util     import *

#----------------------------------------
# A converter for Corona materials from BI
class CoronaMatConvert(bpy.types.Operator):
    bl_label = "Convert Blender Material to Corona Material"
    bl_idname = "corona.mat_convert"
    bl_description = "Convert Blender internal materials to Corona materials"

    scene_wide = bpy.props.BoolProperty( name = "", description = "", default = False)
    
    def execute(self, context):
        scene = context.scene

        diff_map = []
        transl_map = []
        alpha_map = []
        spec_map = []
        spec_col_map = []
        emit_map = []
        normal_map = []

        if self.scene_wide:
            materials = [mat for mat in bpy.data.materials]
        else:
            materials = [context.object.active_material]

        for mat in materials:
            diff_map.clear()
            transl_map.clear()
            alpha_map.clear()
            spec_map.clear()
            spec_col_map.clear()
            emit_map.clear()
            normal_map.clear()

            crn_mat = mat.corona
            for tex in mat.texture_slots:
                if tex:
                    if is_uv_img(tex.name):
                        if tex.use_map_color_diffuse:
                            diff_map.append(tex.name)
                        if tex.use_map_color_spec or tex.use_map_mirror:
                            spec_col_map.append(tex.name)
                        if tex.use_map_specular or tex.use_map_raymir:
                            spec_map.append(tex.name)
                        if tex.use_map_alpha:
                            alpha_map.append(tex.name)
                        if tex.use_map_emit:
                            emit_map.append(tex.name)
                        if tex.use_map_translucency:
                            transl_map.append(tex.name)
                        if tex.use_map_normal:
                            normal_map.append(tex.name)
            
            #Diffuse 
            # Have to save BI diffuse_color before touching Corona Kd
            # or BI diffuse_color will be updated to Kd as Kd is being modified.
            diff_color = mat.diffuse_color
            crn_mat.kd = diff_color
            if len(diff_map) > 0:
                crn_mat.use_map_kd = True
                crn_mat.map_kd.texture = diff_map[0]
                
            #Translucency
            if mat.translucency > 0.0:
                crn_mat.translucency = mat.diffuse_color * mat.translucency
                if len(transl_map) > 0:
                    crn_mat.use_map_translucency = True
                    crn_mat.map_translucency.texture = transl_map[0]
                    
            #Specular
            crn_mat.ks = mat.specular_color
            if mat.specular_intensity > 0.0:
                crn_mat.ns = mat.specular_intensity
                if mat.specular_shader == 'WARDISO':
                    #Convert to Corona's range
                    crn_mat.reflect_glossiness = abs(mat.specular_slope / 0.4 - 1)
                elif mat.specular_shader == 'TOON':
                    #The smaller the size, the glossier it is
                    size = abs(mat.specular_toon_size / 1.53 - 1)
                    #Same goes for smooth value
                    smooth = abs(mat.specular_toon_smooth - 1)
                    crn_mat.reflect_glossiness = size * smooth 
                else:
                    crn_mat.reflect_glossiness = (mat.specular_hardness / 511)
                if len(spec_col_map) > 0:
                    crn_mat.use_map_ks = True
                    crn_mat.map_ks.texture = spec_col_map[0]
                if len(spec_map) > 0:
                    crn_mat.use_map_ns = True
                    crn_mat.map_ns.texture = spec_map[0]
                
            else:
                crn_mat.ns = 0.0
                crn_mat.reflect_glossiness = 0.0
                
            #Transparency
            if mat.use_transparency:
                crn_mat.opacity = Color((mat.alpha, mat.alpha, mat.alpha))
                if mat.transparency_method == 'RAYTRACE':
                    crn_mat.refract_caustics = True
                    crn_mat.refract_thin = False
                    crn_mat.ni = mat.raytrace_transparency.ior
                    crn_mat.refract_glossiness = mat.raytrace_transparency.gloss_factor
                    crn_mat.absorption_color = (mat.diffuse_color * mat.raytrace_transparency.filter)[:]
                    crn_mat.absorption_distance = 100 - mat.raytrace_transparency.depth_max    # Invert
                elif mat.transparency_method == 'Z_TRANSPARENCY':
                    crn_mat.refract_thin = True
                    crn_mat.refract_caustics = False
                    crn_mat.ni = 1.0
                    crn_mat.refract_glossiness = 1.0
                crn_mat.refract_level = mat.raytrace_transparency.fresnel / 5
                    
            #Mirror - override BI specular values
            if mat.raytrace_mirror.use:
                if mat.raytrace_mirror.reflect_factor > 0.0:
                    crn_mat.ns = mat.raytrace_mirror.reflect_factor
                    crn_mat.reflect_glossiness = mat.raytrace_mirror.gloss_factor
                    crn_mat.reflect_fresnel = mat.raytrace_mirror.fresnel
                    crn_mat.ks = mat.mirror_color
                if len(spec_col_map) > 0:
                    crn_mat.use_map_ks = True
                    crn_mat.map_ks.texture = spec_col_map[0]
                if len(spec_map) > 0:
                    crn_mat.use_map_ns = True
                    crn_mat.map_ns.texture = spec_map[0]

            if len(normal_map) > 0:
                crn_mat.use_map_normal = True
                crn_mat.map_normal.texture = normal_map[0]

            #Emit
            if mat.emit > 0.0:
                crn_mat.mtl_type = 'coronalightmtl'
                crn_mat.ke = mat.diffuse_color
                crn_mat.emission_mult = mat.emit
                if len(emit_map) > 0:
                    crn_mat.use_map_ke = True
                    crn_mat.map_ke.texture = emit_map[0]
                    
            #Shadeless
            if mat.use_shadeless:
                crn_mat.mtl_type = 'coronalightmtl'
                crn_mat.ke = mat.diffuse_color
        
        return {'FINISHED'}


# An operator for creating a Corona node tree for the current material 
#    and linking the material to it
class CoronaNewNodeTree( bpy.types.Operator):
    bl_idname = "corona.add_material_nodetree"
    bl_label = "Add Corona Material Node Tree"
    bl_description = "Create a Corona material node tree and link it to the current material"

    def execute( self, context):
        material = context.object.active_material
        nodetree = bpy.data.node_groups.new( '%s Corona Nodetree' % material.name, 'CoronaNodeTree')
        nodetree.use_fake_user = True
        node = nodetree.nodes.new( 'CoronaMtlNode')
        material.corona.node_tree = nodetree.name
        material.corona.node_output = node.name

        return {'FINISHED'}
         
#----------------------------------------
# An operator for creating a Cycles-based node for Corona Ubershader
# Thanks to elindell for this 
class CoronaUbershader( bpy.types.Operator):
    bl_idname = "corona.create_ubershader"
    bl_label = "Create Corona Ubershader"
    bl_description = "Create a Cycles-Corona interchangeable ubershader node network"
    
    def execute(self, context):
        object = context.active_object
        active_mat = context.active_object.active_material
        tree = active_mat.node_tree
        nodes = tree.nodes
        new = nodes.new
        scene = context.scene
        
        main_group = new('ShaderNodeGroup')
        main_node_group = bpy.data.node_groups.new('Corona Nodetree', 'ShaderNodeTree')
        main_group.node_tree = main_node_group
        main_group.width = 250
        
        new = main_group.node_tree.nodes.new
        
        #Group input
        input = new('NodeGroupInput')
        input.location = -2475, 312
        #First power node
        math_pwr = new('ShaderNodeMath')
        math_pwr.location = -2073, -304
        math_pwr.operation = "POWER"
        math_pwr.inputs[1].default_value = 0.50


        math_pwr.inputs[0].default_value = 1.0
        #First math multiply node
        math_mlt = new('ShaderNodeMath')
        math_mlt.location = -1915, -304
        math_mlt.operation = 'MULTIPLY'
        math_mlt.inputs[1].default_value = 0.30
        #First math add node
        math_add = new('ShaderNodeMath')
        math_add.location = -1758, -304
        math_add.operation = 'ADD'
        math_add.inputs[1].default_value = 0.70
        #First subtract node
        math_sub1 = new('ShaderNodeMath')
        math_sub1.location = -1608, -304
        math_sub1.operation = 'SUBTRACT'
        math_sub1.inputs[0].default_value = 1.0
        #Second subtract node - on top
        math_sub2 = new('ShaderNodeMath')
        math_sub2.location = -1643, 678
        math_sub2.operation = 'SUBTRACT'
        math_sub2.inputs[0].default_value = 1.0
        math_sub2.inputs[1].default_value = 0.0
        #First fresnel 
        fresnel1 = new('ShaderNodeFresnel')
        fresnel1.location = -1346, -574
        fresnel1.inputs[0].default_value = 1.45
        #First glossy bsdf - bottom
        glossy1 = new('ShaderNodeBsdfGlossy')
        glossy1.location = -1346, -662
        glossy1.distribution = 'BECKMANN'
        glossy1.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        glossy1.inputs[1].default_value = 0.00
        #Second glossy bsdf - bottom
        glossy2 = new('ShaderNodeBsdfGlossy')
        glossy2.location = -1346, -826
        glossy2.distribution = 'BECKMANN'
        #First diffuse bsdf
        diff1 = new('ShaderNodeBsdfDiffuse')
        diff1.location = -1077, 688
        diff1.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        diff1.inputs[1].default_value = 0.0
        #Second diffuse bsdf
        diff2 = new('ShaderNodeBsdfDiffuse')
        diff2.location = -1077, 376
        #First mix shade - bottom
        mix_sh1 = new('ShaderNodeMixShader')
        mix_sh1.location = -1077, -376
        #Second mix shader - top
        mix_sh2 = new('ShaderNodeMixShader')
        mix_sh2.location = -821, 556
        #First translucent shader
        transl = new('ShaderNodeBsdfTranslucent')
        transl.location = -821, 275
        #Third math sub 
        math_sub3 = new('ShaderNodeMath')
        math_sub3.location = -806, -89
        math_sub3.operation = 'SUBTRACT'
        math_sub3.inputs[0].default_value = 1.0
        #First add shader
        add_sh1 = new('ShaderNodeAddShader')
        add_sh1.location = -821, -523
        #Third mix shader
        mix_sh3 = new('ShaderNodeMixShader')
        mix_sh3.location = -518, 426
        mix_sh3.inputs[0].default_value = 0.0
        #Refraction BSDF
        refr = new('ShaderNodeBsdfRefraction')
        refr.location = -518, -214
        #Fourt mix shader
        mix_sh4 = new('ShaderNodeMixShader')
        mix_sh4.location = -273, 508
        #Second add shader
        add_sh2 = new('ShaderNodeAddShader')
        add_sh2.location = -273, -23
        #Fourth math sub
        math_sub4 = new('ShaderNodeMath')
        math_sub4.location = -258, -341
        math_sub4.inputs[1].default_value = 1.0
        math_sub4.operation = 'SUBTRACT'
        #Fifth mix shader
        mix_sh5 = new('ShaderNodeMixShader')
        mix_sh5.location = -40, 278
        #Emission shader
        emission = new('ShaderNodeEmission')
        emission.location = -40, -52
        #Geometry
        geom = new('ShaderNodeNewGeometry')
        geom.location = 213, 634
        #Third diffuse bsdf
        diff3 = new('ShaderNodeBsdfDiffuse')
        diff3.location = 198, 405
        diff3.inputs[0].default_value = (0.0, 0.0, 0.0, 1.0)
        #Sixth mix shader 
        mix_sh6 = new('ShaderNodeMixShader')
        mix_sh6.location = 198, 166
        mix_sh6.inputs[0].default_value = 0.0
        #Transparent bsdf
        transp = new('ShaderNodeBsdfTransparent')
        transp.location = 198, -109
        #Seventh mix shader
        mix_sh7 = new('ShaderNodeMixShader')
        mix_sh7.location = 477, 434
        #Eighth mix shader  
        mix_sh8 = new('ShaderNodeMixShader')
        mix_sh8.location = 477, 16
        #Group out
        output = new('NodeGroupOutput')
        output.location = 773, 136
        
        #Connect the nodes
        new_link = nodes[main_group.name].node_tree.links.new
        
        new_link(input.outputs[0], math_sub2.inputs[0])
        
        new_link(input.outputs[1], diff2.inputs[0])
        new_link(input.outputs[2], mix_sh3.inputs[0])
        #Translucency node
        new_link(input.outputs[3], transl.inputs[0])
        
        new_link(input.outputs[4], mix_sh4.inputs[0])
        new_link(mix_sh3.outputs[0], mix_sh4.inputs[1])
        new_link(add_sh1.outputs[0], mix_sh4.inputs[2])
        
        new_link(input.outputs[5], glossy2.inputs[0])
        
        #Connect math nodes on bottom
        new_link(input.outputs[6], math_pwr.inputs[0])
        new_link(math_pwr.outputs[0], math_mlt.inputs[0])
        new_link(math_mlt.outputs[0], math_add.inputs[0])
        new_link(math_add.outputs[0], math_sub1.inputs[1])
        new_link(math_sub1.outputs[0], glossy2.inputs[1])
        
        new_link(input.outputs[7], fresnel1.inputs[0])
        new_link(fresnel1.outputs[0], mix_sh1.inputs[0])
        new_link(glossy1.outputs[0], mix_sh1.inputs[1])
        new_link(glossy2.outputs[0], mix_sh1.inputs[2])
        new_link(mix_sh1.outputs[0], add_sh1.inputs[1])
        new_link(mix_sh3.outputs[0], add_sh1.inputs[0])
        
        #Mix Shader 2 inputs
        new_link(diff1.outputs[0], mix_sh2.inputs[1])
        new_link(diff2.outputs[0], mix_sh2.inputs[2])
        new_link(math_sub2.outputs[0], mix_sh2.inputs[0])
        new_link(mix_sh2.outputs[0], mix_sh3.inputs[1])
        new_link(transl.outputs[0], mix_sh3.inputs[2])
        
        
        new_link(input.outputs[8], math_sub2.inputs[1])
        
        new_link(input.outputs[9], refr.inputs[0])
        

        new_link(input.outputs[10], math_sub3.inputs[1])
        new_link(math_sub3.outputs[0], refr.inputs[1])
        
        new_link(input.outputs[11], refr.inputs[2])
        new_link(refr.outputs[0], add_sh2.inputs[1])
        new_link(mix_sh4.outputs[0], add_sh2.inputs[0])
        new_link(add_sh2.outputs[0], mix_sh5.inputs[2])
        new_link(mix_sh4.outputs[0], mix_sh5.inputs[1])
        new_link(input.outputs[8], mix_sh5.inputs[0])
        
        new_link(input.outputs[12], mix_sh6.inputs[0])
        new_link(mix_sh5.outputs[0], mix_sh6.inputs[1])
        new_link(mix_sh7.outputs[0], mix_sh6.inputs[2])
        new_link(diff3.outputs[0], mix_sh7.inputs[2])
        new_link(geom.outputs[6], mix_sh7.inputs[0])
        new_link(emission.outputs[0], mix_sh7.inputs[1])
        
        new_link(input.outputs[13], emission.inputs[0])
        new_link(input.outputs[14], emission.inputs[1])
        
        new_link(input.outputs[15], math_sub4.inputs[1])
        
        new_link(input.outputs[16], transp.inputs[0])
        new_link(mix_sh6.outputs[0], mix_sh8.inputs[1])
        new_link(transp.outputs[0], mix_sh8.inputs[2])
        new_link(math_sub4.outputs[0], mix_sh8.inputs[0])
        
        new_link(mix_sh8.outputs[0], output.inputs[0])
        
        bpy.data.node_groups[main_node_group.name].inputs[0].name = 'Diffuse'
        bpy.data.node_groups[main_node_group.name].inputs[0].default_value = 1.0
        bpy.data.node_groups[main_node_group.name].inputs[1].name = 'Diffuse Color'
        bpy.data.node_groups[main_node_group.name].inputs[2].name = 'Translucency'
        bpy.data.node_groups[main_node_group.name].inputs[2].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[3].name = 'Translucency Color'
        bpy.data.node_groups[main_node_group.name].inputs[4].name = 'Reflection'
        bpy.data.node_groups[main_node_group.name].inputs[4].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[5].name = 'Reflection Color'
        bpy.data.node_groups[main_node_group.name].inputs[6].name = 'Reflection Glossiness'
        bpy.data.node_groups[main_node_group.name].inputs[7].name = 'Reflection IOR'
        bpy.data.node_groups[main_node_group.name].inputs[8].name = 'Refraction'
        bpy.data.node_groups[main_node_group.name].inputs[8].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[9].name = 'Refraction Color'
        bpy.data.node_groups[main_node_group.name].inputs[10].name = 'Refraction Glossiness'
        bpy.data.node_groups[main_node_group.name].inputs[11].name = 'Refraction IOR'
        bpy.data.node_groups[main_node_group.name].inputs[12].name = 'Emission'
        bpy.data.node_groups[main_node_group.name].inputs[12].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[13].name = 'Emission Color'
        bpy.data.node_groups[main_node_group.name].inputs[14].name = 'Emission Strength'
        bpy.data.node_groups[main_node_group.name].inputs[14].default_value = 0.0
        bpy.data.node_groups[main_node_group.name].inputs[15].name = 'Opacity'
        bpy.data.node_groups[main_node_group.name].inputs[15].default_value = 1.0
        bpy.data.node_groups[main_node_group.name].inputs[16].name = 'Opacity Color'
        
        return {'FINISHED'}

def register():
    bpy.utils.register_class( CoronaMatConvert)
    bpy.utils.register_class( CoronaUbershader)
    bpy.utils.register_class( CoronaNewNodeTree)
def unregister():
    bpy.utils.unregister_class( CoronaMatConvert)
    bpy.utils.unregister_class( CoronaUbershader)
    bpy.utils.unregister_class( CoronaNewNodeTree)
