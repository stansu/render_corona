import bpy

#---------------------------------
#Operator for adding render passes
class CoronaAddRenderPass( bpy.types.Operator):
    bl_label = "Add Pass"
    bl_idname = "corona.add_renderpass"
    
    def invoke( self, context, event):
        scene = context.scene
        collection = scene.corona_passes.passes
        index = scene.corona_passes.pass_index

        collection.add()
        num = collection.__len__()
        collection[num-1].name = "Render Pass " + str( num)
    
        return {'FINISHED'}

#-----------------------------------
#Operator for removing render passes
class CoronaRemoveRenderPass( bpy.types.Operator):
    bl_label = "Remove Pass"
    bl_idname = "corona.remove_renderpass"
        
    def invoke( self, context, event):
        scene = context.scene
        collection = scene.corona_passes.passes
        index = scene.corona_passes.pass_index

        collection.remove( index)
        num = collection.__len__()
        if index >= num:
            index = num -1
        if index < 0:
            index = 0
        scene.corona_passes.pass_index = index
            
        return {'FINISHED'}

def register():
    bpy.utils.register_class( CoronaAddRenderPass)
    bpy.utils.register_class( CoronaRemoveRenderPass)
def unregister():
    bpy.utils.unregister_class( CoronaAddRenderPass)
    bpy.utils.unregister_class( CoronaRemoveRenderPass)
