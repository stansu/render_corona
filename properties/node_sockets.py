import bpy
from bpy.types import NodeSocket
from bpy.props import FloatProperty, FloatVectorProperty, EnumProperty
from ..util    import strip_spaces, join_params, join_names_underscore, debug
from .material import CoronaMatProps

# Socket base class.
class CoronaSocket( object):
    # Set to default None.
    prop_type = map_type = socket_val = None

    def get_socket_value( self, node = None):
        ''' Method to return socket's value as a string'''
        return "#CORONA %s %.3f\n" % ( self.prop_type, self.socket_val)
        
    def get_socket_params( self, node = None):
        ''' 
        Method to return socket's values as a list.
        Function will only be called by the node if the socket is linked.
        '''
        linked_node = self.links[0].from_node
        # Return the parameters of the linked node as a list.
        node_params, node_name = linked_node.get_node_params()
        map_directive = '%s :%s\n' % ( self.map_type, node_name)    # ie, map_Kd :AO_Shader
        node_params.append( map_directive)
        return node_params


# Socket base class for :solid color properties.
class CoronaSolidSocket( object):
    prop_type = socket_val = None

    def get_socket_value( self, name):
        ''' Method to return socket's value as a string'''
        return 'newmap {} "{} {:.3f} {:.3f} {:.3f}"\n'.format( ('_').join( (name, 
                                                   strip_spaces( self.name))),
                                                   self.prop_type, 
                                                   self.socket_val[0], 
                                                   self.socket_val[1], 
                                                   self.socket_val[2])

    def get_socket_params( self, name):
        ''' 
        Method to return socket's values as a list.
        Function will only be called by the node if the socket is linked.
        '''
        linked_node = self.links[0].from_node
        # Return the parameters of the linked node.
        node_params, node_name = linked_node.get_node_params()
        map_name = join_names_underscore( name, self.name)
        map_directive = 'newmap %s :%s\n' % ( map_name, node_name)
        node_params.append( map_directive)
        return node_params

        
# AO Shader sockets.
class CoronaAoOccludedSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaAoOccluded"
    bl_label = "Occluded"
    
    prop_type = ':solid'
    
    socket_val = FloatVectorProperty( name = "Occluded", 
                                default = (0, 0, 0), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

        
class CoronaAoUnoccludedSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaAoUnoccluded"
    bl_label = "Unoccluded"

    prop_type = ':solid'   
     
    socket_val = FloatVectorProperty( name = "Unoccluded", 
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)


class CoronaAoDistMapSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaAoDistMap"
    bl_label = "AO Distance Map"

    prop_type = ':solid'   

    socket_val = FloatVectorProperty( name = "Distance Map", 
                                description = "Texture by which to multiply the distance value, allowing it to change spatially",
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
     
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, name):
        return ''
        
# Mix shader sockets.
class CoronaMixASocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaMixA"
    bl_label = "Color"

    prop_type = ':solid'
    
    socket_val = FloatVectorProperty( name = "Color", 
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

            
class CoronaMixBSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaMixB"
    bl_label = "Color"

    prop_type = ':solid'
    socket_val = FloatVectorProperty( name = "Color", 
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

            
class CoronaMixAmountSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaMixAmount"
    bl_label = "Amount"

    prop_type = ':solid'
    socket_val = FloatVectorProperty( name = "Amount", 
                                description = "Color / texture by which to mix two input colors",
                                default = (1, 1, 1), 
                                min = 0, max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)


# Ray switch shader sockets.
class CoronaRaySwitchSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaRaySwitchColor"
    bl_label = "Ray Switch Color"

    prop_type = ':solid'

    socket_val = FloatVectorProperty( name = "Color", 
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
                                
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        
        
# Fresnel shader sockets.
class CoronaFresnelPerpSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaFresnelPerp"
    bl_label = "Perpendicular"

    prop_type = ':solid'
    
    socket_val = FloatVectorProperty( name = "Color", 
                                description = "Color / texture used for grazing angles",
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)


class CoronaFresnelParSocket( NodeSocket, CoronaSolidSocket):
    bl_idname = "CoronaFresnelPar"
    bl_label = "Parallel"

    prop_type = ':solid'
    
    socket_val = FloatVectorProperty( name = "Color", 
                                description = "Color / texture used for rays directly facing the camera",
                                default = (1, 1, 1), 
                                min = 0, 
                                max = 1, 
                                subtype = 'COLOR')
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        

# Material output node sockets.
# Diffuse.
class CoronaDiffuseLevelSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaDiffuseLevel"
    bl_label = "Diffuse Level"

    socket_val = CoronaMatProps.diffuse_level

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
        
class CoronaKdSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaKd"
    bl_label = "Kd"

    map_type = "map_Kd"
                                
    socket_val = CoronaMatProps.kd
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node = None):
        return 'Kd %.3f %.3f %.3f\n' % (self.socket_val[0],
                                      self.socket_val[1],
                                      self.socket_val[2])
        
# Translucency.
class CoronaTranslucencyLevelSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaTranslucencyLevel"
    bl_label = "Translucency Level"

    map_type = "map_translucencyLevel"
    
    socket_val = CoronaMatProps.translucency_level
                                
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = "Translucency Level")

    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node = None):
        return '#CORONA TranslucencyLevel %.3f\n' % self.socket_val
        
        
class CoronaTranslucencySocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaTranslucency"
    bl_label = "Translucency"

    map_type = "map_translucency"
                            
    socket_val =  CoronaMatProps.translucency
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        
    def get_socket_value( self, node = None):
        return '#CORONA Translucency %.3f %.3f %.3f\n' % (self.socket_val[0],
                                                self.socket_val[1],
                                                self.socket_val[2])
# Reflection.
class CoronaKsSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaKs"
    bl_label = "Ks"

    map_type = "map_Ks"
    
    socket_val =  CoronaMatProps.ks
    
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        
    def get_socket_value( self, node = None):
        mult = node.inputs['Reflect Level'].socket_val
        return 'Ks %.3f %.3f %.3f\n' % ( self.socket_val[0] * mult, 
                                         self.socket_val[1] * mult, 
                                         self.socket_val[2] * mult)

class CoronaReflectLevelSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaReflectLevel"
    bl_label = "Level"

    prop_type = "Ns"

    map_type = "map_Ns"
    
    socket_val = CoronaMatProps.ns
    
    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
        

class CoronaReflectGlossSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaReflectGloss"
    bl_label = "Glossiness"

    prop_type = "ReflectGlossiness" 
    
    socket_val =  CoronaMatProps.reflect_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaReflectFresnelSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaReflectFresnel"
    bl_label = "Fresnel"

    prop_type = "ReflectFresnel"
    
    socket_val =  CoronaMatProps.reflect_fresnel

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

class CoronaAnisotropySocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaAnisotropy"
    bl_label = "Anisotropy"

    map_type = "map_anisotropy"
    
    socket_val = CoronaMatProps.anisotropy

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node = None):
        aniso_rot = node.inputs['Anisotropy Rotation'].socket_val
        return '#CORONA Anisotropy %.3f %.3f\n' % ( self.socket_val, aniso_rot)

class CoronaAnisotropyRotSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaAnisotropyRot"
    bl_label = "Anisotropy Rotation"

    map_type = "map_anisorotation"
    
    socket_val = CoronaMatProps.aniso_rotation

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Refraction.
class CoronaNiSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaRefractIOR"
    bl_label = "IOR"

    prop_type = "Ni"
    
    socket_val = CoronaMatProps.ni

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaRefractColorSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaRefractColor"
    bl_label = "Color"

    map_type = "map_refract"
    
    socket_val =  CoronaMatProps.refract

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        
    def get_socket_value( self, node = None):
        mult = node.inputs['Refract Level'].socket_val
        return '#CORONA Refract %.3f %.3f %.3f\n' % ( self.socket_val[0] * mult, 
                                              self.socket_val[1] * mult, 
                                              self.socket_val[2] * mult)

class CoronaRefractGlossSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaRefractGloss"
    bl_label = "Glossiness"

    prop_type = "RefractGlossiness"
    
    socket_val =  CoronaMatProps.refract_glossiness

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    
class CoronaRefractModeSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaRefractMode"
    bl_label = "Mode"
    
    socket_val = EnumProperty( name = "Mode", 
                                description = "Refraction mode",
                                items = [
                                ('twosided', "Thin", "Thin - no refraction"),
                                ('caustics', "Caustics", "Enable refractive caustics (slow)"),
                                ('hybrid', "Hybrid", "Hybrid mode of computing caustics only for direct rays")],
                                default = 'hybrid')

    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node = None):
        return '#CORONA RefractMode %s\n' % self.socket_val

class CoronaRefractLevelSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaRefractLevel"
    bl_label = "Level"
    
    socket_val = CoronaMatProps.refract_level

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)


class CoronaAbsorptionDistSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaAbsorptionDist"
    bl_label = "Absorption Distance"
    
    socket_val = CoronaMatProps.absorption_distance

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0, 0, 0, 0)

class CoronaAbsorptionColorSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaAbsorptionColor"
    bl_label = "Absorption Color"
    
    socket_val = CoronaMatProps.absorption_color

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
        
    def get_socket_value( self, node = None):
        dist = node.inputs['Absorption Distance'].socket_val
        return '#CORONA Attenuation %.3f %.3f %.3f %.3f\n' % ( self.socket_val[0], 
                                                       self.socket_val[1], 
                                                       self.socket_val[2], 
                                                       dist)
        
# Emission.
class CoronaEmissionGlossSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaEmissionGloss"
    bl_label = "Emission Glossiness"

    prop_type = "EmissionGlossiness"
    
    socket_val =  CoronaMatProps.emission_gloss

    def draw( self, context, layout, node, text):

        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
    

class CoronaKeSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaKe"
    bl_label = "Emission Color"

    map_type = "map_Ke"
    
    socket_val =  CoronaMatProps.ke

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)
        
    def get_socket_value( self, node = None):
        mult = node.inputs['Multiplier'].socket_val
        return 'Ke %.3f %.3f %.3f\n' % ( self.socket_val[0] * mult, 
                                         self.socket_val[1] * mult, 
                                         self.socket_val[2] * mult)
        
class CoronaEmissionMultSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaEmissionMult"
    bl_label = "Emission Multiplier"
    
    socket_val = CoronaMatProps.emission_mult

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

    def get_socket_value( self, node = None):
        return ''

        
class CoronaOpacitySocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaOpacity"
    bl_label = "Opacity"

    map_type = "map_opacity"
    
    socket_val =   CoronaMatProps.opacity

    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
        
    def get_socket_value( self, node = None):
        return '#CORONA Opacity %.3f %.3f %.3f\n' % ( self.socket_val[0], 
                                              self.socket_val[1], 
                                              self.socket_val[2])

# Normal socket.
class CoronaNormalSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaNormal"
    bl_label = "Normal"

    map_type = "map_normal"
    
    def draw( self, context, layout, node, text):
        layout.label( text)
    
    def draw_color( self, context, node):
        return (0.67, 0.45, 1, 1)

# Bump socket.
class CoronaBumpSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaBump"
    bl_label = "Bump"

    map_type = "map_bump"
    
    def draw( self, context, layout, node, text):
        layout.label( text)
    
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)
        

# Factor socket.
class CoronaFactorSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaFactor"
    bl_label = "Factor"

    def draw( self, context, layout, node, text):
        layout.label( text)
            
    def draw_color( self, context, node):
        return (0.5, 0.5, 0.5, 1)

# Scattering albedo socket.
class CoronaScatteringAlbedoSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaScattering"
    bl_label = "Scattering"

    map_type = "map_scatteringAlbedo"

    socket_val = CoronaMatProps.scattering_albedo
                                
    def draw( self, context, layout, node, text):
        if self.is_output or self.is_linked:
            layout.label( text)
        else:
            layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0.8, 0.8, 0.5, 1)

    def get_socket_value( self, node = None):
        return '#CORONA ScatteringAlbedo %.3f %.3f %.3f\n' % ( self.socket_val[0], 
                                              self.socket_val[1], 
                                              self.socket_val[2])

# Scattering albedo socket.
class CoronaMeanCosineSocket( NodeSocket, CoronaSocket):
    bl_idname = "CoronaMeanCosine"
    bl_label = "Directionality"

    socket_val = CoronaMatProps.mean_cosine
                                
    def draw( self, context, layout, node, text):
        layout.prop( self, "socket_val", text = text)

    def draw_color( self, context, node):
        return (0, 0, 0, 0)

    def get_socket_value( self, node = None):
        return '#CORONA MeanCosine %.3f\n' % ( self.socket_val)
                                                                
def register():
    bpy.utils.register_class( CoronaAoOccludedSocket)
    bpy.utils.register_class( CoronaAoUnoccludedSocket)
    bpy.utils.register_class( CoronaAbsorptionColorSocket)
    bpy.utils.register_class( CoronaAbsorptionDistSocket)
    bpy.utils.register_class( CoronaMeanCosineSocket)
    bpy.utils.register_class( CoronaScatteringAlbedoSocket)
    bpy.utils.register_class( CoronaRefractLevelSocket)
    bpy.utils.register_class( CoronaRefractModeSocket)
    bpy.utils.register_class( CoronaRefractGlossSocket)
    bpy.utils.register_class( CoronaRefractColorSocket)
    bpy.utils.register_class( CoronaNiSocket)
    bpy.utils.register_class( CoronaAnisotropyRotSocket)
    bpy.utils.register_class( CoronaAnisotropySocket)
    bpy.utils.register_class( CoronaReflectFresnelSocket)
    bpy.utils.register_class( CoronaReflectGlossSocket)
    bpy.utils.register_class( CoronaReflectLevelSocket)
    bpy.utils.register_class( CoronaKsSocket)
    bpy.utils.register_class( CoronaTranslucencySocket)
    bpy.utils.register_class( CoronaKdSocket)
    bpy.utils.register_class( CoronaEmissionGlossSocket)
    bpy.utils.register_class( CoronaEmissionMultSocket)
    bpy.utils.register_class( CoronaKeSocket)
    bpy.utils.register_class( CoronaOpacitySocket)
    bpy.utils.register_class( CoronaMixASocket)
    bpy.utils.register_class( CoronaMixBSocket)
    bpy.utils.register_class( CoronaMixAmountSocket)
    bpy.utils.register_class( CoronaFresnelParSocket)
    bpy.utils.register_class( CoronaFresnelPerpSocket)
    bpy.utils.register_class( CoronaNormalSocket)
    bpy.utils.register_class( CoronaBumpSocket)
    bpy.utils.register_class( CoronaDiffuseLevelSocket)
    bpy.utils.register_class( CoronaTranslucencyLevelSocket)
    bpy.utils.register_class( CoronaRaySwitchSocket)
    bpy.utils.register_class( CoronaFactorSocket)
    
def unregister():
    bpy.utils.unregister_class( CoronaAoOccludedSocket)
    bpy.utils.unregister_class( CoronaAoUnoccludedSocket)
    bpy.utils.unregister_class( CoronaAbsorptionColorSocket)
    bpy.utils.unregister_class( CoronaAbsorptionDistSocket)
    bpy.utils.unregister_class( CoronaMeanCosineSocket)
    bpy.utils.unregister_class( CoronaScatteringAlbedoSocket)
    bpy.utils.unregister_class( CoronaRefractLevelSocket)
    bpy.utils.unregister_class( CoronaRefractModeSocket)
    bpy.utils.unregister_class( CoronaRefractGlossSocket)
    bpy.utils.unregister_class( CoronaRefractColorSocket)
    bpy.utils.unregister_class( CoronaNiSocket)
    bpy.utils.unregister_class( CoronaAnisotropyRotSocket)
    bpy.utils.unregister_class( CoronaAnisotropySocket)
    bpy.utils.unregister_class( CoronaReflectFresnelSocket)
    bpy.utils.unregister_class( CoronaReflectGlossSocket)
    bpy.utils.unregister_class( CoronaReflectLevelSocket)
    bpy.utils.unregister_class( CoronaKsSocket)
    bpy.utils.unregister_class( CoronaTranslucencySocket)
    bpy.utils.unregister_class( CoronaKdSocket)
    bpy.utils.unregister_class( CoronaEmissionGlossSocket)
    bpy.utils.unregister_class( CoronaEmissionMultSocket)
    bpy.utils.unregister_class( CoronaKeSocket)
    bpy.utils.unregister_class( CoronaOpacitySocket)
    bpy.utils.unregister_class( CoronaMixASocket)
    bpy.utils.unregister_class( CoronaMixBSocket)
    bpy.utils.unregister_class( CoronaMixAmountSocket)
    bpy.utils.unregister_class( CoronaFresnelParSocket)
    bpy.utils.unregister_class( CoronaFresnelPerpSocket)
    bpy.utils.unregister_class( CoronaNormalSocket)
    bpy.utils.unregister_class( CoronaBumpSocket)
    bpy.utils.unregister_class( CoronaDiffuseLevelSocket)
    bpy.utils.unregister_class( CoronaTranslucencyLevelSocket)
    bpy.utils.unregister_class( CoronaRaySwitchSocket)
    bpy.utils.unregister_class( CoronaFactorSocket)
