import bpy
from bpy.props import FloatProperty, IntProperty, FloatVectorProperty
from bpy.props import EnumProperty, BoolProperty, StringProperty, PointerProperty
from ..util    import node_enumerator

def view3D_mat_update(self, context):
    context.object.active_material.diffuse_color = context.object.active_material.corona.kd

    
#------------------------------------
# Texture properties
#------------------------------------
class CoronaTexProps( bpy.types.PropertyGroup):

    show_settings = BoolProperty( name = "Show Texture Settings",
                                description = "",
                                default = False)

    
    texture =  StringProperty( name = "Texture", 
                                description = "Texture to influence attribute - only UV image maps are supported", 
                                default = "")
                                
    intensity = FloatProperty( name = "Intensity", 
                                description = "Latitude-longitude map relative brightness",
                                default = 1.0,
                                min = 0.0,
                                soft_max = 5)
    
    uOffset = FloatProperty( name = "Offset U", 
                                description = "Map U offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    vOffset = FloatProperty( name = "Offset V", 
                                description = "Map V offset",
                                default = 0,
                                min = 0.0,
                                max = 1)

    uScaling = FloatProperty( name = "Scaling U", 
                                description = "Map U scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)

    vScaling = FloatProperty( name = "Scaling V", 
                                description = "Map V scaling",
                                default = 1,
                                min = 0.0,
                                soft_max = 10)
                                
#------------------------------------
# Material properties
#------------------------------------
class CoronaMatProps(bpy.types.PropertyGroup):
    # Nodes
    node_tree = StringProperty( name = "Node Tree",
                                description = "Material node tree to link to the current material")

    node_output = StringProperty( name = "Output Node",
                                description = "Material node tree output node to link to the current material")
    
    # Material preview.
    preview_quality = FloatProperty( name = "Preview Quality", 
                                description = "Quality of material preview. Raising this value will increase render time for material preview", 
                                default = 0.3, 
                                min = 0.0, 
                                max = 10, 
                                step = 10, 
                                precision = 1)
                                
    mtl_type = EnumProperty( name = "Corona Material", 
                                description = "Corona material type",
                                items = [
                                ('coronamtl', 'CoronaMtl', 'Corona surface shading material'),
                                ('coronalightmtl', 'CoronaLightMtl', 'Corona mesh light material'),
                                ('coronavolumemtl', 'CoronaVolumeMtl', 'Corona volume material'),
                                ('coronaportalmtl', 'CoronaPortalMtl', 'Corona portal material')],
                                default = 'coronamtl') 
                                
    # Diffuse properties.
    kd  = FloatVectorProperty( name = "Diffuse Color", 
                                description = "Diffuse color of material", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0, 
                                update = view3D_mat_update)
                                
    diffuse_level = FloatProperty( name = "Level", 
                                description = "Influence of diffuse color", 
                                default = 1.0, 
                                min = 0.0, 
                                max = 1.0)

    # Translucency properties.
    translucency =  FloatVectorProperty( name = "Translucency Color", 
                                description = "Translucency color of material", 
                                default = (0.0, 0.0, 0.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)
    
    translucency_level = FloatProperty( name = "Level", 
                                description = "Influence of Translucency", 
                                default = 0.0, 
                                min = 0.0, 
                                max = 1.0)
    
    # Reflection properties.
    ks =  FloatVectorProperty( name = "Reflection Color", 
                                description = "Reflection color of material", 
                                default = (1, 1, 1), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)

    ns = FloatProperty( name = "Level", 
                                description = "Reflectivity of material", 
                                default = 0, 
                                min = 0, 
                                max = 1)
    
    reflect_glossiness =  FloatProperty( name = "Glossiness", 
                                description ="Glossiness of reflective material", 
                                default = 1.0, 
                                min = 0.0, 
                                max = 1.0)
    
    reflect_fresnel =  FloatProperty( name = "Fresnel", 
                                description = "Index of refraction for fresnel reflections", 
                                default = 1.0, 
                                min = 1.0, 
                                max = 100.0  )
    
    anisotropy = FloatProperty( name = "Anisotropy", 
                                description = "Anisotropy of reflective material", 
                                default = 0.5, 
                                min = 0.0, 
                                max = 1.0)
    
    aniso_rotation = FloatProperty( name = "Anisotropy Rotation", 
                                description = "Anisotropy rotation", 
                                default = 0.0, 
                                min = 0.0,
                                 max = 1.0)

    # Emission properties.
    ke =  FloatVectorProperty( name = "Emission Color", 
                                description = "Emission color of material", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)
    
    emission_mult = FloatProperty( name = "Emission Multiplier", 
                                description = "Emission energy multiplier", 
                                default = 1.0, 
                                min = 0.0)
    
    emission_gloss =  FloatProperty( name = "Emission Directionality", 
                                description = "Phong exponent describing emission distribution - 0 produces standard diffuse lights, higher values produce spotlights", 
                                default = 0.0, 
                                min = 0.0, 
                                max = 100.0)

    use_ies = BoolProperty( name = "Use IES Profile",
                                description = "Use an IES emisssion profile for Corona Light material",
                                default = False)
                                
    ies_profile = StringProperty( name = "IES File",
                                description = "Path to IES emission profile",
                                default = '',
                                subtype = 'FILE_PATH')

    keep_sharp = BoolProperty( name = "Keep Sharp",
                                description = "Enable fake mode to enhance sharpness of the profile",
                                default = False)

    # Refraction properties.
    ni = FloatProperty( name = "IOR", 
                                description = "Index of refraction for refractive material", 
                                default = 1.3, 
                                min = 1.0, 
                                max = 4.0)
    
    refract =  FloatVectorProperty( name = "Refraction Color", 
                                description = "Refraction color of refractive material", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)
    
    refract_glossiness =  FloatProperty( name = "Glossiness", 
                                description = "Glossiness of refractive material", 
                                default = 1.0, 
                                min = 0.0, 
                                max = 1.0)
    
    # twosidedGlass: 
    # Don't write anything to .mtl file if refraction_caustics enabled
    # 1 = two-sided ( thin, no refraction or caustics) : refraction_thin enabled 
    # 2 = hybrid (one-sided for primary rays, two-sided for secondary) : neither enabled
    refract_level = FloatProperty( name = "Level", 
                                description = "Amount of refraction",
                                default = 0.0,
                                min = 0.0,
                                max = 1.0,
                                precision = 2)

    refract_caustics = BoolProperty( name = "Caustics",
                                description = "Enable refractive caustics (slow)",
                                default = False)

    refract_thin = BoolProperty( name = "Thin",
                                description = "Thin - no refraction",
                                default = False)
    
    absorption_distance = FloatProperty( name = "Distance", 
                                description = "Absorption distance of the refractive or volumetric material (in scene units)", 
                                default = 0.00, 
                                min = 0.0, 
                                max = 1000, 
                                subtype = 'DISTANCE', 
                                unit = 'LENGTH', 
                                step = 4,
                                precision = 4)
    
    absorption_color = FloatVectorProperty( name = "Color", 
                                description = "Absorption color of the refractive or volumetric material (the color a white ray will be after traveling the absorption distance into the medium)", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)

    opacity =   FloatVectorProperty( name = "Opacity Color", 
                                description = "Opacity color of material. White = fully opaque, black = fully transparent", 
                                default = (1.0, 1.0, 1.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)

    scattering_albedo = FloatVectorProperty( name = "Scattering Color", 
                                description = "Sets the material volumetric scattering albedo. When set to non-zero value, volumetric rendering/SSS is enabled", 
                                default = (0.0, 0.0, 0.0), 
                                subtype = "COLOR", 
                                min = 0.0, 
                                max = 1.0)

    mean_cosine = FloatProperty( name = "Directionality", 
                                description = "Sets the volumetric scattering anisotropy. Value 0 means isotropic (=diffuse) scattering, value 0.999 means almost perfect forward scattering, value -0.999 means almost perfect backward scattering",
                                default = 0,
                                min = -0.999,
                                max = 0.999)

    #Texture bools
    use_map_kd = BoolProperty( name = "Use Diffuse Texture", 
                                description = "Use a texture to influence diffuse color", 
                                default = False)
                                
    use_map_ks = BoolProperty( name = "Use Reflection Color Texture",
                                description = "Use a texture to influence reflection color", 
                                default = False)
          
    use_map_opacity = BoolProperty( name = "Use Opacity Texture", 
                                description = "Use a texture to influence opacity", 
                                default = False)
           
    use_map_ns = BoolProperty( name = "Use Reflection Texture", 
                                description = "Use a texture to influence reflection value", 
                                default = False )
                                 
    use_map_ke = BoolProperty( name = "Use Emission Texture", 
                                description = "Use a texture to influence emission", 
                                default = False)
                                             
    use_map_translucency = BoolProperty( name = "Use Translucency Texture", 
                                description = "Use a texture to influence translucency", 
                                default = False)

    use_map_translucency_level = BoolProperty( name = "Use Translucency Level Texture",
                                description = "Use a textrue to influence translucency level",
                                default = False)
                                         
    use_map_refract = BoolProperty( name = "Use Refraction Texture", 
                                description = "Use a texture to influence refraction", 
                                default = False)
                                              
    use_map_normal = BoolProperty( name = "Use Normal Texture", 
                                description = "Use a texture to influence normal", 
                                default = False)

    use_map_bump = BoolProperty( name = "Use Bump Texture", 
                                description = "Use a texture to influence bump", 
                                default = False)
                                              
    use_map_aniso = BoolProperty( name = "Use Anisotropy Texture",
                                description = "Use a texture to influence anisotropy", 
                                default = False)
                    
    use_map_aniso_rot = BoolProperty( name = "Use Anisotropy Rotation Texture",
                                description = "Use a texture to influence anisotropy rotation", 
                                default = False)

    use_map_scattering = BoolProperty( name = "Use Scattering Albedo Texture",
                                description = "Use a texture to influence scattering albedo", 
                                default = False)

    use_map_absorption = BoolProperty( name = "Use Absorption Texture",
                                description = "Use a texture to influence absorption", 
                                default = False)
                                        
    map_kd =  PointerProperty( type = CoronaTexProps)
                                
    map_ks = PointerProperty( type = CoronaTexProps)
    
    map_opacity = PointerProperty( type = CoronaTexProps)
    
    map_ns = PointerProperty( type = CoronaTexProps)
    
    map_ke = PointerProperty( type = CoronaTexProps)
    
    map_translucency = PointerProperty( type = CoronaTexProps)

    map_translucency_level = PointerProperty( type = CoronaTexProps)
    
    map_refract = PointerProperty( type = CoronaTexProps)
    
    map_normal = PointerProperty( type = CoronaTexProps)

    map_bump = PointerProperty( type = CoronaTexProps)

    map_aniso = PointerProperty( type = CoronaTexProps)
                                
    map_aniso_rot = PointerProperty( type = CoronaTexProps)

    map_scattering = PointerProperty( type = CoronaTexProps)

    map_absorption = PointerProperty( type = CoronaTexProps)
    
    #Portal property
    as_portal = BoolProperty( name = "Use material as light portal", 
                                description = "Material will be used as a light portal. Portal material will not be visible to the camera", 
                                default = False)

    rounded_corners = FloatProperty( name = "Rounded Corners", 
                                description = "Rounded corner radius (in mm)",
                                default = 0.0,
                                min = 0.0,
                                subtype = 'DISTANCE',
                                unit = 'LENGTH')

    ray_gi_inv =  BoolProperty( name = "GI Ray", description = "Invisibility of material to GI rays", default = False)

    ray_direct_inv =  BoolProperty( name = "Direct Ray", description = "Invisibility of material to direct rays", default = False)

    ray_refract_inv =  BoolProperty( name = "Refract Ray", description = "Invisibility of material to refraction rays", default = False)

    ray_reflect_inv =  BoolProperty( name = "Reflection Ray", description = "Invisibility of material to reflection rays", default = False)

    ray_shadows_inv =  BoolProperty( name = "Shadow Ray", description = "Invisibility of material to shadow rays", default = False)
                                
def register():
    bpy.utils.register_class( CoronaTexProps)
    bpy.utils.register_class( CoronaMatProps)
    bpy.types.Material.corona = PointerProperty( type = CoronaMatProps)
    
def unregister():
    bpy.utils.unregister_class( CoronaMatProps)
    bpy.utils.unregister_class( CoronaTexProps)
