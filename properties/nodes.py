import bpy
from bpy.types          import NodeTree, Node
from bpy.app.handlers   import persistent
from nodeitems_utils    import NodeCategory, NodeItem
from ..util             import addon_dir, strip_spaces, realpath, join_names_underscore
from ..util             import filter_flatten_params, debug, CrnInfo
from .material          import CoronaMatProps, CoronaTexProps
import nodeitems_utils
import os
import mathutils

#--------------------------------
# Corona node tree.
#--------------------------------
class CoronaNodeTree( NodeTree):
    '''Corona Node Editor'''
    bl_idname = 'CoronaNodeTree'
    bl_label = 'Corona Node Tree'
    bl_icon = 'NODETREE'

    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine 
        return renderer == 'CORONA'

#--------------------------------    
# Base class for Corona nodes.
#--------------------------------
class CoronaNode:
    @classmethod
    def poll( cls, context):
        renderer = context.scene.render.engine 
        return context.bl_idname == "CoronaNodeTree" and renderer == 'CORONA'

    def draw_buttons( self, context, layout):
        pass
        
    def draw_buttons_ext(self, context, layout):
        pass
    
    def copy( self, node):
        pass
    
    def free( self):
        CrnInfo( "Removing Corona node '%s'" % self.bl_label)
    
    def draw_label( self):
        return self.bl_label
    

#--------------------------------
# Image texture shader node.
#--------------------------------
class CoronaTexNode( Node, CoronaNode):
    '''Corona Image Texture Node'''
    bl_idname = "CoronaTexNode"
    bl_label = "Image Texture"
    bl_icon = 'TEXTURE'

    params = []
    
    tex_path = bpy.props.StringProperty( name = "Texture", 
                                         description = "Path to the image texture", 
                                         default = '', 
                                         subtype = 'FILE_PATH')

    node_type = ':bitmap'
    
    intensity = CoronaTexProps.intensity
    uOffset   = CoronaTexProps.uOffset
    vOffset   = CoronaTexProps.vOffset
    uScaling  = CoronaTexProps.uScaling
    vScaling  = CoronaTexProps.vScaling
    
    def init( self, context):
        self.inputs.new( 'CoronaFactor', "Input")
        self.outputs.new( 'NodeSocketColor', "Color")
        
    def draw_buttons( self, context, layout):
        layout.label("Image:")    
        layout.prop( self, "tex_path", text = "")
        col = layout.column()
        col.active = self.inputs['Input'].is_linked == False
        col.prop( self, "intensity")
        col.prop( self, "uOffset")
        col.prop( self, "vOffset")
        col.prop( self, "uScaling")
        col.prop( self, "vScaling")

    def get_node_params( self): 
        ''' Return the Image Texture node's parameters as a tuple (list, string) '''
        if self.tex_path != '':
            name_pointer = join_names_underscore( self.name, str( self.as_pointer()))
            
            path_string = ( '/').join( realpath( self.tex_path).split('\\'))
            file_map = join_names_underscore( name_pointer, "file")
            intensity = self.intensity
            uOffset   = self.uOffset
            vOffset   = self.vOffset
            uScaling  = self.uScaling
            vScaling  = self.vScaling
            
            if self.inputs['Input'].is_linked:
                linked_node = self.inputs['Input'].links[0].from_node
                if linked_node.node_type == 'offset_scaling':
                    intensity = linked_node.intensity
                    uOffset = linked_node.uOffset
                    vOffset = linked_node.vOffset
                    uScaling = linked_node.uScaling
                    vScaling = linked_node.vScaling
                    
            self.params = [ ('').join( ('newmap ',
                            strip_spaces( name_pointer), 
                            ' "%s ' % self.node_type, 
                            '%.3f ' % intensity,
                            '%.3f ' % uOffset,
                            '%.3f ' % vOffset, 
                            '%.3f ' % uScaling,
                            '%.3f ' % vScaling,
                            '\\"%s\\""\n' % path_string))]
            return self.params, name_pointer
        return ''

#--------------------------------
# AO shader node.
#--------------------------------
class CoronaAONode( Node, CoronaNode):
    '''Corona AO Shader Node'''
    bl_idname = "CoronaAONode"
    bl_label = "AO Shader"
    bl_icon = 'SMOOTH'

    params = []
    
    node_type = ':ao'
    
    distance = bpy.props.FloatProperty( name = "Distance",
                                        description = "Maximum distance rays travel before being considered not occluded",
                                        default = 10,
                                        min = 0,
                                        soft_min = 100)

    invert = bpy.props.BoolProperty( name = "Invert", 
                                        description = "Causes the rays to be shot below normal, not above, resulting in computing the occlusion below normal",
                                        default = False)

    
    samples = bpy.props.IntProperty( name = "Samples",
                                        description = "Quality of ambient occlusion calculation",
                                        default = 50,
                                        min = 1,
                                        max = 2048)
    def init( self, context):
        self.inputs.new( 'CoronaAoDistMap', "Distance Map")
        self.inputs.new( 'CoronaAoOccluded', "Occluded")
        self.inputs.new( 'CoronaAoUnoccluded', "Unoccluded")    
        self.outputs.new( 'NodeSocketShader', "Color")
        
    def draw_buttons( self, context, layout):
        layout.prop( self, "invert")
        layout.prop( self, "samples")
        layout.prop( self, "distance")
    
    def get_node_params( self):
        ''' Return the AO Shader node's parameters as a tuple (list, string) '''
        self.params.clear()
        name_pointer = join_names_underscore( self.name, str( self.as_pointer()))
        
        occluded = join_names_underscore( name_pointer, self.inputs['Occluded'].name)
        unoccluded = join_names_underscore( name_pointer, self.inputs['Unoccluded'].name)
        distmap = join_names_underscore( name_pointer, self.inputs['Distance Map'].name)
        for socket in self.inputs:
            if socket.is_linked:
                socket_params = socket.get_socket_params( name_pointer)
                self.params.extend( socket_params)
            else:
                self.params.append( socket.get_socket_value( name_pointer))

        params_string = ['newmap ', 
                         strip_spaces( name_pointer), 
                         ' "%s ' % self.node_type,
                         ':%s ' % occluded,
                         ':%s ' % unoccluded,
                         '%.3f ' % self.distance,
                         'Samples %d ' % self.samples]
        if self.invert:
            params_string.append( 'invert ')
        if self.inputs['Distance Map'].is_linked:
            params_string.append( 'distMap :%s"\n' % distmap)
        else:
            params_string.append( '"\n')
        self.params.append( ('').join( params_string))
        return self.params, name_pointer
        

#--------------------------------    
# Mix shader node.
#--------------------------------
class CoronaMixNode( Node, CoronaNode):
    '''Corona Mix Shader Node'''
    bl_idname = "CoronaMixNode"
    bl_label = "Mix Shader"
    bl_icon = 'SMOOTH'

    params = []
    
    node_type = ':mix'
    
    def init( self, context):
        self.inputs.new( 'CoronaMixA', "Color A")
        self.inputs.new( 'CoronaMixB', "Color B")
        self.inputs.new( 'CoronaMixAmount', "Amount")
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self):
        ''' Return the Mix Shader node's parameters as a tuple (list, string) '''
        self.params.clear()
        name_pointer = join_names_underscore( self.name, str( self.as_pointer()))
        
        mix_color_a = join_names_underscore( name_pointer, self.inputs['Color A'].name)
        mix_color_b = join_names_underscore( name_pointer, self.inputs['Color B'].name)
        mix_color_amount = join_names_underscore( name_pointer, self.inputs['Amount'].name)
        for socket in self.inputs:
            if socket.is_linked:
                socket_params = socket.get_socket_params( name_pointer)
                self.params.extend( socket_params)
            else:
                self.params.append( socket.get_socket_value( name_pointer))

        self.params.append( ('').join( ('newmap ', 
                            strip_spaces( name_pointer), 
                            ' "%s ' % self.node_type,
                            ':%s ' % mix_color_a,
                            ':%s ' % mix_color_b,
                            ':%s "\n' % mix_color_amount)))
        return self.params, name_pointer

#--------------------------------
# Fresnel shader node.
#--------------------------------
class CoronaFresnelNode( Node, CoronaNode):
    '''Corona Fresnel Shader Node'''
    bl_idname = "CoronaFresnelNode"
    bl_label = "Fresnel Shader"
    bl_icon = 'SMOOTH'

    params = []
    
    node_type = ':fresnel'
    
    ior = bpy.props.FloatProperty( name = "IOR", 
                                    description = "How much the parallel color / texture is used (higher IOR = more prominent parallel color)",
                                    default = 1,
                                    min = 1, 
                                    max = 10)
    
    def init( self, context):
        self.inputs.new( 'CoronaFresnelPerp', "Perpendicular")
        self.inputs.new( 'CoronaFresnelPar', "Parallel")
        self.outputs.new( 'NodeSocketShader', "Color")
        
    def draw_buttons( self, context, layout):
        layout.prop( self, "ior")

    def get_node_params( self):
        ''' Return the Fresnel Shader node's parameters as a tuple (list, string) '''
        self.params.clear()
        name_pointer = join_names_underscore( self.name, str(self.as_pointer()))
        
        perpendicular_color = join_names_underscore( name_pointer, self.inputs['Perpendicular'].name)
        parallel_color = join_names_underscore( name_pointer, self.inputs['Parallel'].name)
        for socket in self.inputs:
            if socket.is_linked:
                socket_params = socket.get_socket_params( name_pointer)
                self.params.extend( socket_params)
            else:
                self.params.append( socket.get_socket_value( name_pointer))

        self.params.append( ('').join( ('newmap ', 
                            strip_spaces( name_pointer), 
                            ' "%s ' % self.node_type,
                            '%.3f ' % self.ior,
                            ':%s ' % perpendicular_color,
                            ':%s "\n' % parallel_color)))
        return self.params, name_pointer


#--------------------------------
# Ray switch shader node.
#--------------------------------
class CoronaRaySwitchNode( Node, CoronaNode):
    '''Corona Ray Switch Shader Node'''
    bl_idname = "CoronaRaySwitchNode"
    bl_label = "Ray Switch Shader"
    bl_icon = 'SMOOTH'

    params = []
    
    node_type = ':rayswitch'

    def init( self, context):
        self.inputs.new( 'CoronaRaySwitchColor', "GI")
        self.inputs.new( 'CoronaRaySwitchColor', "Reflect")
        self.inputs.new( 'CoronaRaySwitchColor', "Refract")
        self.inputs.new( 'CoronaRaySwitchColor', "Direct")
        self.outputs.new( 'NodeSocketShader', "Color")

    def get_node_params( self):
        ''' Return the Ray Switch shader node's parameters as a tuple (list, string) '''
        self.params.clear()
        name_pointer = join_names_underscore( self.name, str(self.as_pointer()))
        
        gi_color = join_names_underscore( name_pointer, self.inputs['GI'].name)
        reflect_color = join_names_underscore( name_pointer, self.inputs['Reflect'].name)
        refract_color = join_names_underscore( name_pointer, self.inputs['Refract'].name)
        direct_color = join_names_underscore( name_pointer, self.inputs['Direct'].name)
        
        for socket in self.inputs:
            if socket.is_linked:
                socket_params = socket.get_socket_params( name_pointer)
                self.params.extend( socket_params)
            else:
                self.params.append( socket.get_socket_value( name_pointer))

        self.params.append( ('').join( ('newmap ', 
                            strip_spaces( name_pointer), 
                            ' "%s ' % self.node_type,
                            ':%s ' % gi_color,
                            ':%s ' % reflect_color,
                            ':%s ' % refract_color,
                            ':%s "\n' % direct_color)))
                            
        return self.params, name_pointer

#--------------------------------
# Offset / Scaling node.
#--------------------------------
class CoronaOffsetScaleNode( Node, CoronaNode):
    '''Corona Offset And Scaling Node'''
    bl_idname = "CoronaOffsetScaleNode"
    bl_label = "Offset / Scaling"
    bl_icon = 'MAN_SCALE'

    intensity = CoronaTexProps.intensity
    uOffset   = CoronaTexProps.uOffset
    vOffset   = CoronaTexProps.vOffset
    uScaling  = CoronaTexProps.uScaling
    vScaling  = CoronaTexProps.vScaling
    
    node_type = 'offset_scaling'

    def init( self, context):
        self.outputs.new( 'CoronaFactor', "Output")
        
    def draw_buttons( self, context, layout):
        layout.prop( self, "intensity")
        layout.prop( self, "uOffset")
        layout.prop( self, "vOffset")
        layout.prop( self, "uScaling")
        layout.prop( self, "vScaling")

    def get_node_params( self):
        pass

        
#--------------------------------
# Material output node.
#--------------------------------
class CoronaMtlNode( Node, CoronaNode):
    '''Corona Material Output Node'''
    bl_idname = "CoronaMtlNode"
    bl_label = "Corona Mtl"
    bl_icon = 'SMOOTH'

    params = []
    
    as_portal       = CoronaMatProps.as_portal
    rounded_corners = CoronaMatProps.rounded_corners
    ray_gi_inv      = CoronaMatProps.ray_gi_inv
    ray_direct_inv  = CoronaMatProps.ray_direct_inv
    ray_refract_inv = CoronaMatProps.ray_refract_inv
    ray_reflect_inv = CoronaMatProps.ray_reflect_inv
    ray_shadows_inv = CoronaMatProps.ray_shadows_inv
    
    def init( self, context):
        self.inputs.new( 'CoronaDiffuseLevel', "Diffuse Level")
        self.inputs.new( 'CoronaKd', "Diffuse Color")
        self.inputs.new( 'CoronaTranslucencyLevel', "Translucency Level")
        self.inputs.new( 'CoronaTranslucency', "Translucency Color")
        self.inputs.new( 'CoronaReflectLevel', "Reflect Level")
        self.inputs.new( 'CoronaKs', "Reflect Color")
        self.inputs.new( 'CoronaReflectGloss', "Reflect Gloss")
        self.inputs.new( 'CoronaReflectFresnel', "Fresnel")
        self.inputs.new( 'CoronaAnisotropy', "Anisotropy")
        self.inputs.new( 'CoronaAnisotropyRot', "Anisotropy Rotation")
        self.inputs.new( 'CoronaRefractLevel', "Refract Level")
        self.inputs.new( 'CoronaRefractColor', "Refract Color")
        self.inputs.new( 'CoronaRefractGloss', "Refract Gloss")
        self.inputs.new( 'CoronaRefractIOR', "Refract IOR")
        self.inputs.new( 'CoronaRefractMode', "Refract Mode")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        self.inputs.new( 'CoronaNormal', "Normal")
#        self.inputs.new( 'CoronaBump', "Bump")
        
    def draw_buttons( self, context, layout):
        img = bpy.data.images.get('CORONA32')
        if img is not None:
            icon = layout.icon(img)
            layout.label(text="", icon_value=icon)
        layout.prop( self, "as_portal")
        layout.prop( self, "rounded_corners")
        layout.label( "Ray Invisibility:")
        row = layout.row()
        row.prop( self, "ray_gi_inv")
        row.prop( self, "ray_direct_inv")
        row = layout.row()
        row.prop( self, "ray_reflect_inv")
        row.prop( self, "ray_refract_inv")
        layout.prop( self, "ray_shadows_inv")

    def get_node_params( self):
        ''' Return the Material node's parameters as a string '''
        self.params.clear()
        no_test = {'Diffuse Level',
                   'Reflect Level',
                   'Refract Level', 
                   'Anisotropy Rotation', 
                   'Absorption Distance', 
                   'Normal',
                   'Bump'}
                   
        for socket in self.inputs:
            if socket.name not in no_test:
                # Append a string of only the property
                self.params.append( socket.get_socket_value( self))
            if socket.map_type is not None and socket.is_linked:
                # Map-able property
                self.params.extend( socket.get_socket_params( self))
                                 
        if self.as_portal:
            self.params.append( '#CORONA Portal \n')
        if self.rounded_corners > 0.0:
            self.params.append( '#CORONA RoundedCorners %.2f \n' % self.rounded_corners)
        # Ray invisibility
        inv_list = [ 'ray_gi_inv', 
                     'ray_direct_inv', 
                     'ray_reflect_inv', 
                     'ray_refract_inv', 
                     'ray_shadows_inv']
        attr_list = [c for c in inv_list if getattr( self, c)]
        if len( attr_list) > 0:
            self.params.append( 'Invisible ')
            for i in attr_list:
                self.params.append( '%s ' % i)

        # Filter out repeated items and flatten the list into a string
        return filter_flatten_params( self.params)

#--------------------------------
# Light material output node.
#--------------------------------
class CoronaLightMtlNode( Node, CoronaNode):
    '''Corona Light Material Output Node'''
    bl_idname = "CoronaLightMtlNode"
    bl_label = "Corona LightMtl"
    bl_icon = 'SMOOTH'

    params = []
    
    use_ies         = CoronaMatProps.use_ies
    ies_profile     = CoronaMatProps.ies_profile
    keep_sharp      = CoronaMatProps.keep_sharp
    ray_gi_inv      = CoronaMatProps.ray_gi_inv
    ray_direct_inv  = CoronaMatProps.ray_direct_inv
    ray_refract_inv = CoronaMatProps.ray_refract_inv
    ray_reflect_inv = CoronaMatProps.ray_reflect_inv
    ray_shadows_inv = CoronaMatProps.ray_shadows_inv
    
    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaOpacity', "Opacity")
        
    def draw_buttons( self, context, layout):
        img = bpy.data.images.get('CORONA32')
        if img is not None:
            icon = layout.icon(img)
            layout.label(text="", icon_value=icon)
        layout.prop( self, "use_ies")
        col = layout.column()
        col.active = self.use_ies
        col.prop( self, "ies_profile")
        col.prop( self, "keep_sharp")
        
        layout.label( "Ray Invisibility:")
        row = layout.row()
        row.prop( self, "ray_gi_inv")
        row.prop( self, "ray_direct_inv")
        row = layout.row()
        row.prop( self, "ray_reflect_inv")
        row.prop( self, "ray_refract_inv")
        layout.prop( self, "ray_shadows_inv")

    def get_node_params( self):
        self.params.clear()
        for socket in self.inputs:
            if socket.name != 'Multiplier':
                # Append a string of only the property
                self.params.append( socket.get_socket_value( self))
            if socket.map_type is not None and socket.is_linked:
                # Map-able property
                self.params.extend( socket.get_socket_params( self))
        
        if self.use_ies and self.ies_profile != '':
            matrix = mathutils.Matrix.Identity(4)
            matrix = ' '.join([str(i) for m in matrix for i in m])
            self.params.append( 'IesProfile %s %s %s' % (realpath( self.ies_profile),
                                                         matrix,
                                                         str(self.keep_sharp).lower()))
        # Ray invisibility
        inv_list = [ 'ray_gi_inv', 
                     'ray_direct_inv', 
                     'ray_reflect_inv', 
                     'ray_refract_inv', 
                     'ray_shadows_inv']
        attr_list = [c for c in inv_list if getattr( self, c)]
        if len( attr_list) > 0:
            self.params.append( 'Invisible ')
            for i in attr_list:
                self.params.append( '%s ' % i)
        
        return filter_flatten_params( self.params)


#--------------------------------
# Volume material output node.
#--------------------------------
class CoronaVolumeMtlNode( Node, CoronaNode):
    '''Corona Volume Material Output Node'''
    bl_idname = "CoronaVolumeMtlNode"
    bl_label = "Corona Volume Mtl"
    bl_icon = 'SMOOTH'

    params = []
    
    ray_gi_inv      = CoronaMatProps.ray_gi_inv
    ray_direct_inv  = CoronaMatProps.ray_direct_inv
    ray_refract_inv = CoronaMatProps.ray_refract_inv
    ray_reflect_inv = CoronaMatProps.ray_reflect_inv
    ray_shadows_inv = CoronaMatProps.ray_shadows_inv
    
    def init( self, context):
        self.inputs.new( 'CoronaKe', "Emission Color")
        self.inputs.new( 'CoronaEmissionGloss', "Directionality")
        self.inputs.new( 'CoronaEmissionMult', "Multiplier")
        self.inputs.new( 'CoronaAbsorptionColor', "Absorption Color")
        self.inputs.new( 'CoronaAbsorptionDist', "Absorption Distance")
        self.inputs.new( 'CoronaScattering', "Scattering Color")
        self.inputs.new( 'CoronaMeanCosine', "Scattering Direction")
        
    def draw_buttons( self, context, layout):
        img = bpy.data.images.get('CORONA32')
        if img is not None:
            icon = layout.icon(img)
            layout.label(text="", icon_value=icon)
            
        layout.label( "Ray Invisibility:")
        row = layout.row()
        row.prop( self, "ray_gi_inv")
        row.prop( self, "ray_direct_inv")
        row = layout.row()
        row.prop( self, "ray_reflect_inv")
        row.prop( self, "ray_refract_inv")
        layout.prop( self, "ray_shadows_inv")

    def get_node_params( self):
        ''' Return the Material node's parameters as a string '''
        self.params.clear()
        no_test = {'Absorption Distance',
                   'Multiplier'}
                   
        for socket in self.inputs:
            if socket.name not in no_test:
                # Append a string of only the property
                self.params.append( socket.get_socket_value( self))
            if socket.map_type is not None and socket.is_linked:
                # Map-able property
                self.params.extend( socket.get_socket_params( self))

        self.params.extend( ['#CORONA Opacity 0 0 0\n', '#CORONA RefractMode caustics\n'])
        
        # Ray invisibility
        inv_list = [ 'ray_gi_inv', 
                     'ray_direct_inv', 
                     'ray_reflect_inv', 
                     'ray_refract_inv', 
                     'ray_shadows_inv']
        attr_list = [c for c in inv_list if getattr( self, c)]
        if len( attr_list) > 0:
            self.params.append( 'Invisible ')
            for i in attr_list:
                self.params.append( '%s ' % i)

        # Filter out repeated items and flatten the list into a string
        return filter_flatten_params( self.params) 
#--------------------------------
# Node category for extending the Add menu, toolbar panels 
#   and search operator    
# Base class for node categories
#--------------------------------
class CoronaNodeCategory( NodeCategory):
    @classmethod 
    def poll(cls, context):
        renderer = context.scene.render.engine
        return context.space_data.tree_type == 'CoronaNodeTree' and renderer == 'CORONA'

#--------------------------------
# Corona node categories
# identifier, label, items list
#--------------------------------
corona_node_categories = [
    CoronaNodeCategory("SHADERUTILS", "Shader Utils", items = [
        NodeItem( "CoronaAONode"),
        NodeItem( "CoronaMixNode"),
        NodeItem( "CoronaFresnelNode"),
        NodeItem( "CoronaRaySwitchNode")]),
    CoronaNodeCategory("TEXTURE", "Texture", items = [
        NodeItem( "CoronaTexNode"),
        NodeItem( "CoronaOffsetScaleNode")]),
    CoronaNodeCategory("OUTPUT", "Output", items = [
        NodeItem( "CoronaMtlNode"),
        NodeItem( "CoronaLightMtlNode"),
        NodeItem( "CoronaVolumeMtlNode")])]

@persistent
def scene_loaded(dummy):
    # Load images as icons 
    icon16 = bpy.data.images.get('CORONA16')
    icon32 = bpy.data.images.get('CORONA32')
    if icon16 is None:
        img = bpy.data.images.load(os.path.join( os.path.join( addon_dir, 'icons'), 'corona16.png'))
        img.name = 'CORONA16'
        img.use_alpha = True
        img.user_clear() 
    # remove scene_update handler
    elif "corona16" not in icon16.keys():
        icon16["corona16"] = True
        for f in bpy.app.handlers.scene_update_pre:
            if f.__name__ == "scene_loaded":
                bpy.app.handlers.scene_update_pre.remove(f)
    if icon32 is None:
        img = bpy.data.images.load(os.path.join( os.path.join( addon_dir, 'icons'), 'corona32.png'))
        img.name = 'CORONA32'
        img.use_alpha = True
        img.user_clear() # Won't get saved into .blend files
    # remove scene_update handler
    elif "corona32" not in icon32.keys():
        icon32["corona32"] = True
        for f in bpy.app.handlers.scene_update_pre:
            if f.__name__ == "scene_loaded":
                bpy.app.handlers.scene_update_pre.remove(f)
                
def register():
    bpy.app.handlers.load_post.append(scene_loaded)
    bpy.app.handlers.scene_update_pre.append(scene_loaded)
    nodeitems_utils.register_node_categories("CORONA_NODES", corona_node_categories)
    bpy.utils.register_class( CoronaNodeTree)
    bpy.utils.register_class( CoronaTexNode)
    bpy.utils.register_class( CoronaAONode)
    bpy.utils.register_class( CoronaMtlNode)
    bpy.utils.register_class( CoronaLightMtlNode)
    bpy.utils.register_class( CoronaVolumeMtlNode)
    bpy.utils.register_class( CoronaMixNode)
    bpy.utils.register_class( CoronaRaySwitchNode)
    bpy.utils.register_class( CoronaOffsetScaleNode)

def unregister():
    nodeitems_utils.unregister_node_categories("CORONA_NODES")
    bpy.app.handlers.load_post.remove(scene_loaded)
    bpy.utils.unregister_class( CoronaNodeTree)
    bpy.utils.unregister_class( CoronaTexNode)
    bpy.utils.unregister_class( CoronaAONode)
    bpy.utils.unregister_class( CoronaMtlNode)
    bpy.utils.unregister_class( CoronaLightMtlNode)
    bpy.utils.unregister_class( CoronaVolumeMtlNode)
    bpy.utils.unregister_class( CoronaMixNode)
    bpy.utils.unregister_class( CoronaRaySwitchNode)
    bpy.utils.unregister_class( CoronaOffsetScaleNode)
    

