import bpy
from . import camera, material, nodes, node_sockets, object, particle, passes, scene, world

def register():
    camera.register()
    material.register()
    nodes.register()
    node_sockets.register()
    object.register()
    particle.register()
    passes.register()
    scene.register()
    world.register()
    

def unregister():
    world.unregister()
    camera.unregister()
    material.unregister()
    nodes.unregister()
    node_sockets.unregister()
    object.unregister()
    particle.unregister()
    passes.unregister()
    scene.unregister()
    
    
