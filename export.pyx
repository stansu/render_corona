import bpy
import bpy_extras.io_utils
import os, subprocess
import time
import struct
import math, mathutils
from extensions_framework   import util as efutil    
from .util                  import *
cdef extern from "objUtil.h":
    void objWriter(const char *verts, 
                   const char *vn, 
                   const char *vt, 
                   const char *faces, 
                   const char *filepath)

#---------------------------------------------
# General utilities and settings
#---------------------------------------------

cdef cpy_write( file, string):
    file.write( string)

cdef veckey3d(v):
    cdef double x
    cdef double y
    cdef double z
    x = v.x
    y = v.y
    z = v.z
    return x, y, z
    
cdef calc_decrement( double first, double last, int segments):
    return (( 1 - (last / first)) / segments)
    
cdef get_hairs( obj, scene, psys, crv_ob, crv_data, mat_name):
    cdef int p, steps, num_parents, num_children, step
    cdef double p_rad, rad_decrement
    cdef double root_size, tip_size
    root_size = psys.settings.corona.root_size
    tip_size = psys.settings.corona.tip_size
    # Set the render resolution of the particle system
    psys.set_resolution( scene, obj, 'RENDER')
    steps = 2 ** psys.settings.render_step + 1
    num_parents = len( psys.particles)
    num_children = len( psys.child_particles)
    curves = []
    crv_meshes = []
    c_append = curves.append
    cm_append = crv_meshes.append
    transform = obj.matrix_world.inverted()       
    for p in range( 0, num_parents + num_children):   
        crv = bpy.data.curves.new( 'hair_curve_%d' % p, 'CURVE')
        curves.append( crv)
        crv.splines.new( 'NURBS')
        points = crv.splines[0].points
        crv.splines[0].points.add( steps - 1)
        crv.splines[0].use_endpoint_u = True
        crv.splines[0].order_u = 4
        crv.dimensions = '3D'
        crv.fill_mode = 'FULL'
        if psys.settings.corona.shape == 'thick':
            crv.bevel_depth = psys.settings.corona.scaling
            crv.bevel_resolution = psys.settings.corona.resolution
        else: 
            crv.extrude = psys.settings.corona.scaling
        crv.resolution_u = 1   
        p_rad = 1.0
        rad_decrement = calc_decrement( root_size, tip_size, steps)
        for step in range(0, steps):
            co = psys.co_hair( obj, p, step)
            points[step].co = mathutils.Vector( ( co.x, co.y, co.z, 1.0)) 
            points[step].radius = p_rad
            p_rad -= rad_decrement
        crv.transform( transform)
        # Create an object for the curve, add the material, then convert to mesh
        crv_ob.data = bpy.data.curves[crv.name]
        crv_ob.data.materials.append( bpy.data.materials[mat_name])
        mesh = crv_ob.to_mesh( scene, True, 'RENDER', calc_tessface = True)
        crv_meshes.append( mesh)
        crv_ob.data = crv_data
        bpy.data.curves.remove( crv)
    psys.set_resolution( scene, obj, 'PREVIEW')
    return crv_meshes 

cdef mesh_triangulate(me):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.triangulate(bm, faces=bm.faces, quad_method = 2, ngon_method = 2)
    bm.to_mesh(me)
    bm.free()
    del bm

cdef decide_format( self, obj_path, obj, scene, curves):
    if scene.corona.binary_obj:
        write_binary_obj( self, obj_path, obj, scene, curves)
    elif use_def_mb( obj, scene):
        write_def_obj( self, obj_path, obj, scene, curves)
    else:
        write_obj( self, obj_path, obj, scene, curves)
        
cdef name_compat(name):
    if name is None:
        return 'None'
    else:
        return name.replace(' ', '_')

cdef test_nurbs_compat(ob):
    if ob.type != 'CURVE':
        return False
    for nu in ob.data.splines:
        if nu.point_count_v == 1 and nu.type != 'BEZIER':  # not a surface and not bezier
            return True
    return False
    
#---------------------------------------------
#   Geometry export   
#---------------------------------------------
def export(self, scene, curves):
    if scene.corona.export_path == '':
        CrnError("Corona export path is not set!  Please set the export path before exporting objects.")
        return
    #First, add all instanced geometry to a set so we only export once
    self._inst_obs = {(bpy.data.objects[ob.corona.instance_mesh]) for ob in scene.objects if (is_proxy(ob, scene) and not ob.corona.use_external)}
    self._inst_obs.update( get_all_duplis( scene))
    self._inst_obs.update( get_all_psysobs())

    #Call the function and actually write everything now
    filepath = realpath(scene.corona.export_path)
    _write(self, scene, filepath, curves)

#---------------------------------------------
def _write( self, scene, filepath, curves):
    # Exit edit mode before exporting, so current object states are exported properly.
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode='OBJECT')

    mesh_dir = os.path.join( filepath, "meshes")
    if not os.path.exists( mesh_dir):
        os.mkdir( mesh_dir)
        
    bin_obj = scene.corona.binary_obj
    obj_ext = '.objb' if bin_obj else '.obj'
    
    for obj in scene.objects:
        if do_export( obj, scene):
            full_path = os.path.join( mesh_dir, obj.name.replace(" ", "_")) + obj_ext
            if scene.corona.obj_export_mode == 'PARTIAL' and os.path.isfile( full_path):
                # Don't write the file if it exists
                continue
            elif scene.corona.obj_export_mode == 'SELECTED':
                # Only write the file if it the object is selected
                if obj.select == True:
                    decide_format( self, full_path, obj, scene, curves)
                else:
                    continue
            else:
                # If writing all, or the mode is 'PARTIAL' and the file does not exist
                decide_format( self, full_path, obj, scene, curves)


#--------------------------------------
# .obj write function
#--------------------------------------
def write_obj(self, filepath, ob, scene, curves):
    """
    Basic write function. The context and options must be already set. 
    This will write one .obj file per object
    """
    # Initialize totals, these are updated each object
    cdef int totverts, totuvco, totno, face_vert_index, vi
    cdef char* path
    cdef char* verts_p
    cdef char* tex_p
    cdef char* normals_p
    cdef char* faces_p
    totverts = totuvco = totno = 1

    # Write using Python if exporting hair, to keep memory usage from becoming a problem
    EXPORT_HAIR = scene.corona.export_hair and has_hairsys( ob)
    if EXPORT_HAIR:
        crv = curves[0]
        crv_ob = curves[1]
    
    face_vert_index = 1

    globalNormals = {}

    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    meshes = []
    ob_mat = ob.matrix_world

    # Don't export dupli parents, only dupli child. 
    if ob.dupli_type != 'NONE': 
        return
        
    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return
        
    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add(ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len(ob.particle_systems) > 0 and not render_emitter(ob):
        export_mesh = False
   
    if export_mesh:
        try:
            mesh = ob.to_mesh(scene, True, 'RENDER', calc_tessface=False)
            if ob.corona.triangulate:
                mesh_triangulate( mesh)
        except RuntimeError:
            mesh = None
        if mesh is None:
            return
        
    # Create a list of meshes.
    if export_mesh:
        meshes.append(mesh)

    CrnProgress("Writing .obj file ", (ob.name.replace(" ", "_") + ".obj"))    
    
    # Add any hair meshes, if there are hair particle systems.
    if EXPORT_HAIR:
        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                    CrnProgress("Exporting hair system", psys.name)
                    mat_index = psys.settings.material - 1
                    material = ob.material_slots[mat_index].name
                    meshes.extend(get_hairs(ob, scene, psys, crv_ob, crv, material))
        # Open a file for writing hairs
        try:
            file = open( filepath, "w", encoding = "utf8")
        except:
            CrnProgress( "Cannot create file %s. Check directory permissions." % filepath)
            return

    # Try for some optimizations:
    # Append all vert / vertex texture coords / face text lines and write them all 
    verts = []
    verts_n = []
    verts_t = []
    faces = []
    edge_list = []

    # Time the export.
    time1 = time.time()
    
    for me in meshes:       
        verts.clear()
        verts_n.clear()
        verts_t.clear()
        faces.clear()
        edge_list.clear()
        #Export UVs
        faceuv = len(me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]

        # Make sure there is something to write
        if not (len(face_index_pairs) + len(me.vertices)):  
            # clean up
            bpy.data.meshes.remove(me)
            return  # dont bother with this mesh.

        # Default to exporting normals for Corona
        if face_index_pairs:
            me.calc_normals()

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, 
                                    hash(uv_texture[a[1]].image), 
                                    a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, 
                                    a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.
        
        # Vertices
        for v in me_verts:
            verts.append('v %.6f %.6f %.6f\n' % v.co[:])

        if EXPORT_HAIR:
            write_verts = ('').join(verts)  
            cpy_write( file, write_verts)
            del write_verts
        # UV
        if faceuv:
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_face_mapping = [None] * len(face_index_pairs)

            uv_dict = {}  # could use a set() here
            for f, f_index in face_index_pairs:
                uv_ls = uv_face_mapping[f_index] = []
                append = uv_ls.append
                for uv_index, l_index in enumerate(f.loop_indices):
                    uv = uv_layer[l_index].uv

                    uvkey = uv[0], uv[1]
                    try:
                        uv_k = uv_dict[uvkey]
                    except:
                        uv_k = uv_dict[uvkey] = len(uv_dict)
                        verts_t.append('vt %.6f %.6f\n' % uv[:])
                    append(uv_k)

            uv_unique_count = len(uv_dict)

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping

        if not faceuv:
            f_image = None
            verts_t.append('vt 0 0 0\n')

        if EXPORT_HAIR:
            write_tex = ('').join(verts_t)  
            cpy_write( file, write_tex)
            del write_tex
        
        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                vertices = f.vertices
                for v_idx in vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        verts_n.append('vn %.6f %.6f %.6f\n' % noKey)
                        
            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    verts_n.append('vn %.6f %.6f %.6f\n' % noKey)

        if EXPORT_HAIR:
            write_normals = ('').join(verts_n)  
            cpy_write( file, write_normals)
            del write_normals
            
        for f, f_index in face_index_pairs:
            f_smooth = f.use_smooth
            f_mat = min(f.material_index, len(materials) - 1)

            if faceuv:
                tface = uv_texture[f_index]
                f_image = tface.image

            # MAKE KEY
            if faceuv and f_image:  # Object is always true.
                key = material_names[f_mat], f_image.name
            else:
                key = material_names[f_mat], None  # No image, use None instead.

            # CHECK FOR CONTEXT SWITCH
            if key == contextMat:
                pass  # Context already switched, dont do anything
            else:
                if key[0] is None and key[1] is None:
                    # Write a null material, since we know the context has changed.
                    faces.append("usemtl None\n")
                
                else:
                    mat_data = mtl_dict.get(key)
                    if not mat_data:
                    
                        # First add to global dict so we can export to mtl
                        # Then write mtl

                        # Make a new names from the mat and image name,
                        # converting any spaces to underscores with name_compat.

                        # If none image dont bother adding it to the name
                        # Try to avoid as much as possible adding texname (or other things)
                        # to the mtl name (see [#32102])...
                        mtl_name = key[0] #"%s" % name_compat(key[0])
                        mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image
                        mtl_rev_dict[mtl_name] = key
                    
                    faces.append("usemtl %s\n" % mat_data[0])

            contextMat = key

            f_v = [(vi, me_verts[v_idx]) for vi, v_idx in enumerate(f.vertices)]

            # Write faces
            faces.append('f')

            if faceuv:
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        faces.append(" %d/%d/%d" %
                                   (v.index + totverts,
                                    totuvco + uv_face_mapping[f_index][vi],
                                    globalNormals[veckey3d(v.normal)],
                                    ))

                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        faces.append(" %d/%d/%d" %
                                   (v.index + totverts,
                                    totuvco + uv_face_mapping[f_index][vi],
                                    no,
                                    ))

                face_vert_index += len(f_v)

            else:
                # No UV's
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        faces.append(" %d/1/%d" % (
                                   v.index + totverts,
                                   globalNormals[veckey3d(v.normal)],
                                   ))
                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        faces.append(" %d/1/%d" % (v.index + totverts, no))

            faces.append('\n')
            #End of face_index_pairs 'for' loop

        if EXPORT_HAIR:
            write_faces = ('').join(faces)  
            cpy_write( file, write_faces)
            del write_faces

        if not EXPORT_HAIR:
            # Write the vertices
            write_verts_p = ('').join(verts).encode('UTF-8')
            verts_p = write_verts_p
            
            # Write vertex normals
            write_normals_p = ('').join(verts_n).encode('UTF-8')
            normals_p = write_normals_p
            
            # Write tex coords
            write_tex_p = ('').join(verts_t).encode('UTF-8')
            tex_p = write_tex_p
            
            # Write faces
            write_faces_p = ('').join(faces).encode('UTF-8')
            faces_p = write_faces_p
            
            ### Write everything to the file, if not writing hair ###
            filepath_p = filepath.encode('UTF-8')
            path = filepath_p
            objWriter( verts_p, normals_p, tex_p, faces_p, path)
            
        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)
            
    # Close the file if writing hair.
    if EXPORT_HAIR:
        file.close()

    # Clear leftover data
    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()
    self._inst_obs.clear()
    
    CrnInfo( "OBJ Export time: %.4f" % (time.time() - time1))


########################################
# BINARY OBJ FILE WRITER - Write these values for declarations:
# LINE_V       = 33,
# LINE_VT      = 44,
# LINE_VN      = 55,
#            
# LINE_TRI     = 66,
# LINE_QUAD    = 77,
# LINE_USEMTL  = 88,
# LINE_COMMENT = 99,
########################################
def write_binary_obj(self, filepath, ob, scene, curves):
    """
    Binary .OBJ file write function
    """
    # Initialize totals, these are updated each object
    cdef int totverts, totuvco, totno, face_vert_index, vi
    totverts = totuvco = totno = face_vert_index = 1

    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    meshes = []
    ob_mat = ob.matrix_world
    globalNormals = {}

    EXPORT_HAIR = scene.corona.export_hair and has_hairsys( ob)
    if EXPORT_HAIR:
        crv = curves[0]
        crv_ob = curves[1]
        
    # Don't export dupli parents, only dupli child. 
    if ob.dupli_type != 'NONE': 
        return
        
    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return
        
    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add( ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len( ob.particle_systems) > 0 and not render_emitter( ob):
        export_mesh = False 
    if export_mesh:
        try:
            mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface=False)
            if ob.corona.triangulate:
                mesh_triangulate( mesh)
        except RuntimeError:
            mesh = None
        if mesh is None:
            return
        
    # Create a list of meshes.
    if export_mesh:
        meshes.append(mesh)

    CrnProgress( "Writing .objb file ", (ob.name.replace(" ", "_") + ".objb"))  
    
    if EXPORT_HAIR:
        for mod in ob.modifiers:
            if mod.type == 'PARTICLE_SYSTEM' and mod.show_render:
                psys = mod.particle_system
                if psys.settings.type == 'HAIR' and psys.settings.render_type == 'PATH':
                    CrnProgress( "Exporting hair system", psys.name)
                    mat_index = psys.settings.material - 1
                    material = ob.material_slots[mat_index].name
                    meshes.extend( get_hairs(ob, scene, psys, crv_ob, crv, material))
    
    #Open the file for writing
    file = open( filepath, "wb")
    fw = file.write
    pack = struct.pack
      
    # Time the export.
    time1 = time.time()

    for me in meshes:
        #Export UVs
        faceuv = len( me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]
        # faces = [ f for f in me.tessfaces ]

        if not (len(face_index_pairs) + len(me.vertices)):  # Make sure there is somthing to write

            # clean up
            bpy.data.meshes.remove(me)

            return  # dont bother with this mesh.

        #Default to exporting normals for Corona, so test for face_index_pairs only
        #if face_index_pairs:
        me.calc_normals()

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, hash(uv_texture[a[1]].image), a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.
        
        # Write verts
        for v in me_verts:
            fw(pack('B',33))
            fw(pack('fff', v.co[0], v.co[1], v.co[2]))

        # UV
        if faceuv:
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_face_mapping = [None] * len(face_index_pairs)

            uv_dict = {}  # could use a set() here
            for f, f_index in face_index_pairs:
                uv_ls = uv_face_mapping[f_index] = []
                for uv_index, l_index in enumerate(f.loop_indices):
                    uv = uv_layer[l_index].uv

                    uvkey = uv[0], uv[1]

                    try:
                        uv_k = uv_dict[uvkey]
                    except:
                        uv_k = uv_dict[uvkey] = len(uv_dict)
                        #fw('vt %.6f %.6f\n' % uv[:])
                        fw(pack('B',44))
                        fw(pack('fff', uv[0], uv[1], 0))
                    uv_ls.append(uv_k)

            uv_unique_count = len(uv_dict)

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping

        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                for v_idx in f.vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        #Write vertex normals
                        fw(pack('B',55))
                        fw(pack('fff', noKey[0], noKey[1], noKey[2]))
                        
            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    fw(pack('B',55))
                    fw(pack('fff', noKey[0], noKey[1], noKey[2]))

        if not faceuv:
            f_image = None
            fw(pack('B',44))
            fw(pack('LLL', 0, 0, 0))


        for f, f_index in face_index_pairs:
            f_smooth = f.use_smooth
            f_mat = min(f.material_index, len(materials) - 1)

            if faceuv:
                tface = uv_texture[f_index]
                f_image = tface.image

            # MAKE KEY
            if faceuv and f_image:  # Object is always true.
                key = material_names[f_mat], f_image.name
            else:
                key = material_names[f_mat], None  # No image, use None instead.

            # CHECK FOR CONTEXT SWITCH
            if key == contextMat:
                pass  # Context already switched, dont do anything
            else:
                if key[0] is None and key[1] is None:
                    # Write a null material, since we know the context has changed.
                    #Write 'None' material
                    fw(pack('B',88))
                    fw(bytes('None', 'latin-1'))
                    fw(pack('B',0))
                
                else:
                    mat_data = mtl_dict.get(key)
                    if not mat_data:
                    
                        # First add to global dict so we can export to mtl
                        # Then write mtl

                        # Make a new names from the mat and image name,
                        # converting any spaces to underscores with name_compat.

                        # If none image dont bother adding it to the name
                        # Try to avoid as much as possible adding texname (or other things)
                        # to the mtl name (see [#32102])...
                        mtl_name = key[0] #"%s" % name_compat(key[0])
                        mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image
                        mtl_rev_dict[mtl_name] = key

                    # Write the material for the faces
                    fw(pack('B',88))
                    fw(bytes(mat_data[0], 'latin-1'))
                    fw(pack('B',0))

            f_v = [(vi, me_verts[v_idx]) for vi, v_idx in enumerate(f.vertices)]

            # Write faces
            if faceuv:
                ccvx = []
                ccvt = []
                ccvn = []
                # Need to index faces from 0, not from 1
                # So subtract 1 from each value before writing
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        ccvt.append(totuvco + uv_face_mapping[f_index][vi] - 1)
                        ccvn.append(globalNormals[veckey3d(v.normal)] - 1)

                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        ccvt.append(totuvco + uv_face_mapping[f_index][vi] - 1)
                        ccvn.append(no - 1)
                if len(ccvx) == 3:
                    # Tri
                    fw(pack('B',66))
                    fw(pack('LLL', ccvx[0], ccvx[1], ccvx[2]))
                    fw(pack('LLL', ccvn[0], ccvn[1], ccvn[2]))
                    fw(pack('LLL', ccvt[0], ccvt[1], ccvt[2]))
                else:
                    # Quad
                    fw(pack('B',77))
                    fw(pack('LLLL', ccvx[0], ccvx[1], ccvx[2], ccvx[3]))
                    fw(pack('LLLL', ccvn[0], ccvn[1], ccvn[2], ccvn[3]))
                    fw(pack('LLLL', ccvt[0], ccvt[1], ccvt[2], ccvt[3]))
                 
                face_vert_index += len(f_v)

            else:
                ccvx = []
                ccvn = []
                # No UV's
                if f_smooth:  # Smoothed, use vertex normals
                    for vi, v in f_v:               
                        ccvx.append(v.index + totverts - 1)
                        ccvn.append(globalNormals[veckey3d(v.normal)] - 1)
                else:  # No smoothing, face normals
                    no = globalNormals[veckey3d(f.normal)]
                    for vi, v in f_v:
                        ccvx.append(v.index + totverts - 1)
                        ccvn.append(no - 1)
                if len(ccvx) == 3:
                    # Tri
                    fw(pack('B',66))
                    fw(pack('LLL', ccvx[0], ccvx[1], ccvx[2]))
                    fw(pack('LLL', ccvn[0], ccvn[1], ccvn[2]))
                    fw(pack('LLL', 0, 0, 0))
                else:
                    # Quad
                    fw(pack('B',77))
                    fw(pack('LLLL', ccvx[0], ccvx[1], ccvx[2], ccvx[3]))
                    fw(pack('LLLL', ccvn[0], ccvn[1], ccvn[2], ccvn[3]))
                    fw(pack('LLLL', 0, 0, 0, 0))


        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)

    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()

    file.close()

    self._inst_obs.clear()

    CrnInfo( "OBJB Export time: %.4f" % (time.time() - time1))

#--------------------------------------
# Deformation MB .obj write function
#--------------------------------------
def write_def_obj(self, filepath, ob, scene, curves):
    """
    Write function for deformed motion-blurred object. 
    """
    # Initialize totals, these are updated each object
    cdef int totverts, totuvco, totno, face_vert_index, vi, mesh_idx
    cdef char* path
    cdef char* verts_p
    cdef char* tex_p
    cdef char* normals_p
    cdef char* faces_p
    totverts = totuvco = totno = face_vert_index = 1
    meshes = []
    globalNormals = {}

    # Each value of startMeshVerts will be a list [v, vt, vn]  
    #   for a face vertex at index of mesh_idx in start_mesh.  
    # The key is mesh_idx.
    startMeshVerts = {}
    
    # A Dict of Materials
    # (material.name, image.name):matname_imagename # matname_imagename has gaps removed.
    mtl_dict = {}

    # Used to reduce the usage of matname_texname materials, which can become annoying in case of
    # repeated exports/imports, yet keeping unique mat names per keys!
    # mtl_name: (material.name, image.name)
    mtl_rev_dict = {}

    # Don't export dupli parents, only dupli child. 
    if ob.dupli_type != 'NONE': 
        return
        
    # Don't export proxy objects, only the instanced mesh (and only once)
    if ob.corona.is_proxy:
        return
        
    if ob in self._inst_obs:
        if ob not in self._exported_obs:
            self._exported_obs.add(ob)
        else:
            # It's already been exported
            return

    # Test for particle systems. Render the emitter if enabled.
    export_mesh = True
    if len(ob.particle_systems) > 0 and not render_emitter(ob):
        export_mesh = False
   
    if export_mesh:
        try:
            frame_orig = scene.frame_current
            frame_set = scene.frame_set
            offset = 0.5 + ( scene.corona.frame_offset / 2)
            interval = scene.render.fps *  ( 1 / scene.corona.shutter_speed)
            start = frame_orig - ( interval * ( 1 - offset))
            end = frame_orig + ( interval * offset)

            # Have to set subframe separately.
            frame_set( int(start), subframe = ( start % 1))
            start_mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface = True)

            frame_set( int( end), subframe = ( end % 1))
            end_mesh = ob.to_mesh( scene, True, 'RENDER', calc_tessface= True)

            # Triangulate for deformed objects
            mesh_triangulate( start_mesh)
            mesh_triangulate( end_mesh)
            
            # Reset timeline and return.
            frame_set( frame_orig)
            
        except RuntimeError:
            start_mesh = None
        if start_mesh is None:
            return
        
    # Create a list of meshes.
    meshes.extend( [ start_mesh, end_mesh])

    CrnProgress( "Writing .obj file ", ( ob.name.replace(" ", "_") + ".obj"))    
    
    # Try for some optimizations:
    # Append all vert / vertex texture coords / face text lines and write them all 
    verts = []
    verts_n = []
    verts_t = []
    faces = []
    edge_list = []

    # Time the export.
    time1 = time.time()
    
    for me in meshes:   
        mesh_idx = 1    # Reset mesh index 
        
        #Export UVs
        faceuv = len(me.uv_textures) > 0
        if faceuv:
            uv_texture = me.uv_textures.active.data[:]
            uv_layer = me.uv_layers.active.data[:]

        me_verts = me.vertices[:]

        # Make our own list so it can be sorted to reduce context switching
        face_index_pairs = [ ( face, index) for index, face in enumerate( me.polygons)]

        # Make sure there is something to write
        if not (len(face_index_pairs) + len(me.vertices)):  
            # clean up
            bpy.data.meshes.remove(me)
            return  # dont bother with this mesh.

        # Default to exporting normals for Corona
        if face_index_pairs:
            me.calc_normals()

        materials = me.materials[:]
        material_names = [m.name if m else None for m in materials]

        # avoid bad index errors
        if not materials:
            materials = [None]
            material_names = [name_compat(None)]

        # Sort by Material, then images
        # so we dont over context switch in the obj file.
        if faceuv:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, 
                                    hash(uv_texture[a[1]].image), 
                                    a[0].use_smooth))
        elif len(materials) > 1:
            face_index_pairs.sort(key=lambda a: (a[0].material_index, 
                                    a[0].use_smooth))
        else:
            # no materials
            face_index_pairs.sort(key=lambda a: a[0].use_smooth)

        # Set the default mat to no material and no image.
        contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
        contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.
        
        # Vertices
        for v in me_verts:
            verts.append('v %.6f %.6f %.6f\n' % v.co[:])

        # UV
        if faceuv:
            # in case removing some of these dont get defined.
            uv = uvkey = uv_dict = f_index = uv_index = uv_ls = uv_k = None

            uv_face_mapping = [None] * len(face_index_pairs)

            uv_dict = {}  # could use a set() here
            for f, f_index in face_index_pairs:
                uv_ls = uv_face_mapping[f_index] = []
                append = uv_ls.append
                for uv_index, l_index in enumerate(f.loop_indices):
                    uv = uv_layer[l_index].uv

                    uvkey = uv[0], uv[1]
                    try:
                        uv_k = uv_dict[uvkey]
                    except:
                        uv_k = uv_dict[uvkey] = len(uv_dict)
                        verts_t.append('vt %.6f %.6f\n' % uv[:])
                    append(uv_k)

            uv_unique_count = len(uv_dict)

            del uv, uvkey, uv_dict, f_index, uv_index, uv_ls, uv_k
            # Only need uv_unique_count and uv_face_mapping

        if not faceuv:
            f_image = None
            verts_t.append('vt 0 0 0\n')
        
        # NORMAL, Smooth/Non smoothed.
        for f, f_index in face_index_pairs:
            if f.use_smooth:
                vertices = f.vertices
                for v_idx in vertices:
                    v = me_verts[v_idx]
                    noKey = veckey3d(v.normal)
                    if noKey not in globalNormals:
                        globalNormals[noKey] = totno
                        totno += 1
                        verts_n.append('vn %.6f %.6f %.6f\n' % noKey)
                        
            else:
                # Hard, 1 normal from the face.
                noKey = veckey3d(f.normal)
                if noKey not in globalNormals:
                    globalNormals[noKey] = totno
                    totno += 1
                    verts_n.append('vn %.6f %.6f %.6f\n' % noKey)

        # Faces.
        # Store face data on START mesh
        # Only add face data to faces list on the END mesh, looking up corresponding vert indices 
        #   from start_mesh
        if me == start_mesh: 
            for f, f_index in face_index_pairs:
                f_smooth = f.use_smooth
                f_mat = min(f.material_index, len(materials) - 1)

                if faceuv:
                    tface = uv_texture[f_index]
                    f_image = tface.image

                # MAKE KEY
                if faceuv and f_image:  # Object is always true.
                    key = material_names[f_mat], f_image.name
                else:
                    key = material_names[f_mat], None  # No image, use None instead.

                # CHECK FOR CONTEXT SWITCH
                if key == contextMat:
                    pass  # Context already switched, dont do anything

                contextMat = key

                f_v = [ ( vi, me_verts[ v_idx]) for vi, v_idx in enumerate( f.vertices)]
                
                if faceuv:
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                        totuvco + uv_face_mapping[ f_index][ vi],
                                        globalNormals[ veckey3d( v.normal)] ]
                                        
                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                        totuvco + uv_face_mapping[ f_index][ vi],
                                        globalNormals[ veckey3d( f.normal)] ]
                            
                            mesh_idx += 1
                            
                    face_vert_index += len(f_v)

                else:
                    # No UV's
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts,
                                       1,
                                       globalNormals[ veckey3d( v.normal)] ]

                            mesh_idx += 1

                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            startMeshVerts[ mesh_idx] = [ v.index + totverts, 
                                       1,
                                       globalNormals[ veckey3d( f.normal)] ]

                            mesh_idx += 1
                            
                #End of face_index_pairs 'for' loop
                
        else:   
            # Iterating over end_mesh
            # Look up the corresponding vertices from start_mesh 
            #   and append them to faces list 
            for f, f_index in face_index_pairs:
                f_smooth = f.use_smooth
                f_mat = min(f.material_index, len(materials) - 1)

                if faceuv:
                    tface = uv_texture[f_index]
                    f_image = tface.image

                # MAKE KEY
                if faceuv and f_image:  # Object is always true.
                    key = material_names[f_mat], f_image.name
                else:
                    key = material_names[f_mat], None  # No image, use None instead.

                # CHECK FOR CONTEXT SWITCH
                if key == contextMat:
                    pass  # Context already switched, dont do anything
                else:
                    if key[0] is None and key[1] is None:
                        # Write a null material, since we know the context has changed.
                        faces.append("usemtl None\n")
                    
                    else:
                        mat_data = mtl_dict.get(key)
                        if not mat_data:
                        
                            # First add to global dict so we can export to mtl
                            # Then write mtl

                            # Make a new names from the mat and image name,
                            # converting any spaces to underscores with name_compat.

                            # If none image dont bother adding it to the name
                            # Try to avoid as much as possible adding texname (or other things)
                            # to the mtl name (see [#32102])...
                            mtl_name = key[0] #"%s" % name_compat(key[0])
                            mat_data = mtl_dict[key] = mtl_name, materials[f_mat], f_image
                            mtl_rev_dict[mtl_name] = key
                        
                        faces.append("usemtl %s\n" % mat_data[0])

                contextMat = key

                f_v = [ ( vi, me_verts[ v_idx]) for vi, v_idx in enumerate( f.vertices)]
                
                # Write faces
                faces.append('a 1')

                if faceuv:
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0], 
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( v.normal)] 
                                        ))
                                        
                            mesh_idx += 1
                            
                    else:  # No smoothing, face normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( f.normal)]
                                        ))
                                        
                            mesh_idx += 1
                            
                    face_vert_index += len(f_v)

                else:
                    # No UV's
                    if f_smooth:  # Smoothed, use vertex normals
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0], 
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( v.normal)] 
                                        ))

                            mesh_idx += 1
                            
                    else:  # No smoothing, face normals
                        no = globalNormals[ veckey3d( f.normal)]
                        no_end = globalNormals[ veckey3d( f_end.normal)]
                        for vi, v in f_v:
                            faces.append(" %d:%d/%d/%d:%d" %
                                        ( startMeshVerts[ mesh_idx][0],
                                        v.index + totverts,
                                        startMeshVerts[ mesh_idx][1],
                                        startMeshVerts[ mesh_idx][2],
                                        globalNormals[ veckey3d( f.normal)]
                                        ))
                                       
                            mesh_idx += 1
                
                faces.append('\n')
                #End of face_index_pairs 'for' loop
    
        # Make the indices global rather then per mesh
        totverts += len(me_verts)
        if faceuv:
            totuvco += uv_unique_count

        # clean up
        bpy.data.meshes.remove(me)
        
    # Write the vertices
    write_verts_p = ('').join(verts).encode('UTF-8')
    verts_p = write_verts_p
    
    # Write vertex normals
    write_normals_p = ('').join(verts_n).encode('UTF-8')
    normals_p = write_normals_p

    
    # Write tex coords
    write_tex_p = ('').join(verts_t).encode('UTF-8')
    tex_p = write_tex_p
    
    # Write faces
    write_faces_p = ('').join(faces).encode('UTF-8')
    faces_p = write_faces_p
    
    ### Write everything to the file, if not writing hair ###
    filepath_p = filepath.encode('UTF-8')
    path = filepath_p
    objWriter( verts_p, normals_p, tex_p, faces_p, path)
    
    # Clear leftover data
    if ob.dupli_type != 'NONE':
        ob.dupli_list_clear()
    self._inst_obs.clear()
    
    CrnInfo( "OBJ Export time: %.4f" % (time.time() - time1))
